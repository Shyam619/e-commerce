import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {

  static final _dbName = 'myDatabase.db';
  static final _dbVersion = 1;
  static final _tableName = 'myTable';
  static final  columnId = 'id';
  static final  columnName = 'name';
  static final  columnName1 = 'heading';
  static final  columnName2 = 'title';
  static final  columnName3 = 'price';
  static final  columnName4 = 'size';
  static final  columnName5 = 'description';
  static final  columnName6 = 'image';
  static final  columnName7 = 'color';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;

  Future<Database> get database async => _database??= await _initiateDatabase();


  Future<Database> _initiateDatabase() async {

    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path,_dbName);
    return await openDatabase(path, version: _dbVersion, onCreate: _onCreate);


  }

  ///creating a table with number of columns.
  Future _onCreate(Database db, int version) async {
    db.execute(
        '''
        CREATE TABLE $_tableName(
         $columnId INTEGER PRIMARY KEY,
         $columnName1 TEXT NOT NULL,
         $columnName2 TEXT NOT NULL,
         $columnName3 INTEGER,
         $columnName4 INTEGER,
         $columnName5 TEXT NOT NULL,
         $columnName6 TEXT NOT NULL,
         $columnName7 TEXT NOT NULL 
         ) 
        
        ''');
  }

  ///insert data in table
  Future<int> insert(Map<String, dynamic> row) async {
    Database? db = await instance.database;
    return await db.insert(_tableName, row);

  }

  ///getting all  the stored data
  Future<List<Map<String, dynamic>>> queryAll() async {
    Database? db =await instance.database;
    return await db.query(_tableName);

  }

  ///updating the already existing data
  Future<int> update(Map<String,dynamic> row) async {
    Database? db = await instance.database;
    int id = row[columnId];
    return await db.update(_tableName, row, where: '$columnId = ?', whereArgs: [id]);

  }

  ///delete the data by using their primary key 'id'
  Future delete(int id) async {
    Database? db = await instance.database;
    return await db.delete(_tableName,where: '$columnId =?',whereArgs: [id]);

  }

}