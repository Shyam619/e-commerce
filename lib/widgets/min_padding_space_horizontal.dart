
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/material.dart';

class MiniPaddingSpaceHorizontal extends StatelessWidget {
  MiniPaddingSpaceHorizontal();
  @override
  Widget build(BuildContext context) {
    return SizedBox(width: DEFAULT_PADDING_MIN);
  }
}
