
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:flutter/material.dart';

class DefaultAppBar extends StatefulWidget {

  DefaultAppBar({this.buttonText, this.onPressed, this.size});

  final String? buttonText;
  final VoidCallback? onPressed;
  final Size? size;

  @override
  State<DefaultAppBar> createState() => _DefaultAppBarState();
}

class _DefaultAppBarState extends State<DefaultAppBar> {
   bool? isLoginActive;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('AppBar'),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.arrow_back_rounded),
        ),
        actions: [

          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
         isLoginActive == false ?
        Container():
         Center(
           child: InkWell(
             onTap: (){
               setState((){
                 isLoginActive = !isLoginActive!;
               });
             },
             child: Padding(
               padding: const EdgeInsets.symmetric(horizontal: 8.0),
               child: Text(
                 "Login",
                 style: Theme.of(context)
                     .textTheme
                     .subtitle1!
                     .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
               ),
             ),
           ),
         )
        ],
      ),
    );
  }
}
