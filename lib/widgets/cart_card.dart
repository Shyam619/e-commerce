import 'dart:core';
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class CartCard extends StatefulWidget {
  Product singleProductData;

  /*static int ;*/

  CartCard( {Key? key, required this.singleProductData}) : super(key: key);

  @override
  _CartCardState createState() => _CartCardState();
}

class _CartCardState extends State<CartCard> {
  int quantity = 0;
  static int subTotal = 0;
  late Product singleProductData;



  @override
  void initState() {
    super.initState();
    singleProductData=widget.singleProductData;
  }
  @override
  void dispose() {
    super.dispose();
  }

  void calculationPlus(int quantity, int price) {
    subTotal += quantity*price;
  }



  Widget productImage() => CachedNetworkImage(
    alignment: Alignment.center,
    imageUrl:
    "https://www.pngall.com/wp-content/uploads/4/Leather-Bag-PNG.png",
    imageBuilder: (context, imageProvider) => Container(
        width: double.infinity,
        height: DASHBOARD_CARD_PRODUCT_SIZE,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS),
              topRight:
              Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS)),
          image: DecorationImage(
            image: NetworkImage(
                "https://www.pngall.com/wp-content/uploads/4/Leather-Bag-PNG.png"),
            fit: BoxFit.cover,
          ),
        )),
    placeholder: (context, url) =>
        Center(child: CircularProgressIndicator()),
    errorWidget: (context, url, error) => Icon(Icons.error),
  );

  Widget incDec() {
    Widget _incrementButton(int index) {
      return Container(
        height: CART_INC_DEC_SIZE,
        width: CART_INC_DEC_SIZE,
        child: FloatingActionButton(
            elevation: 0.5,

            onPressed: () {
              setState(() {
                quantity++;
                calculationPlus(quantity, singleProductData.price);

              });
            },
            child: Icon(Icons.add, color: PRIMARY_COLOR),
            backgroundColor: WHITE_COLOR),
      );
    }

    Widget _decrementButton(int index) {
      return Container(
        height: CART_INC_DEC_SIZE,
        width: CART_INC_DEC_SIZE,
        child: FloatingActionButton(
            elevation: 0.5,
            onPressed: () {

              quantity==0 ? Container() : setState(() {
                quantity--;

              });


            },
            child: new Icon(Icons.remove, color: PRIMARY_COLOR),
            backgroundColor: WHITE_COLOR),
      );
    }

    return Container(
      height: 40.0,
      width: 100.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _decrementButton(0),
          Text(
            quantity.toString(),
            style: TextStyle(fontSize: 18.0, color: PRIMARY_TEXT_COLOR_BLACK),
          ),
          _incrementButton(0),
        ],
      ),
    );
  }
  Widget productName() => Text(
    singleProductData.title,
    textAlign: TextAlign.center,
    style: Theme.of(context).textTheme.headline6!.copyWith(
      fontWeight: FontWeight.w400,
      color: PRIMARY_TEXT_COLOR_BLACK,
    ),
  );

  Widget productDesc() => Text(
    singleProductData.description,
    textAlign: TextAlign.left,
    style: Theme.of(context).textTheme.bodyText1!.copyWith(
      fontWeight: FontWeight.w500,
      color: TEXT_COLOR_ASH_LITE,
    ),
  );

  Widget productPrice() => Text(
    "Price: "+singleProductData.price.toString(),
    textAlign: TextAlign.left,
    style: Theme.of(context).textTheme.bodyText1!.copyWith(
      fontWeight: FontWeight.w600,
      color: Colors.black,
    ),
  );

  @override
  Widget build(BuildContext context) {

    return Container(
      height: 180.0,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(DEFAULT_BORDER_RADIUS),
        ),
        child: InkWell(
          child: Row(
            children: [
              Expanded(flex: 1, child: productImage()),
              Expanded(
                flex: 3,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(child: productName()),
                      Expanded(child: productDesc()),
                      SizedBox(height: DEFAULT_SIZED_BOX_MIN_HEIGHT,),
                      Row(
                        children: [
                          incDec(),
                          Spacer(),
                          productPrice()
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
