import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:flutter/material.dart';

class DataSearch extends SearchDelegate<String> {
  late final Product product;

  final searchData = [
    "Mumbai",
    "Delhi",
    "Bangalore",
    "Hyderabad",
    "Ahmedabad	",
    "Chennai	",
    "Kolkata",
    "Surat",
    "Pune",
    "Jaipur",
    "Lucknow",
    "Kanpur",
    "Nagpur",
    "Indore",
    "Agra",
    "Coimbatore	",
    "Nashik",
    "Ranchi",
  ];
  final recentSearchData = [
    "a",
    "b",
    "c",
    "d",
  ];

  @override
  void close(BuildContext context, String result) {
    super.close(context, result);
  }


  @override
  List<Widget>? buildActions(BuildContext context) {
    // Actions for appar
    return [
      IconButton(
          onPressed: () {
            query = "";
          },
          icon: Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    // icon in leading
    return IconButton(
        onPressed: () {
          close(context, "result");
        },
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ));
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // show when search is operated
    final suggestionList = query.isEmpty ? recentSearchData : searchData.where((p) => p.toLowerCase().startsWith(query)).toList();

    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        title: Text(
          suggestionList[index],
          style: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
        ),
      ),
      itemCount: suggestionList.length,
    );
  }
}
