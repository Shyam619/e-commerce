
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/material.dart';

class MiniButton extends StatefulWidget{

  final String buttonText;
  final VoidCallback? onPressed;
  final Size? size;

 MiniButton({required this.buttonText,this.onPressed,this.size});

  @override
  _MiniButtonState createState()=> _MiniButtonState(); 
}

class _MiniButtonState extends State <MiniButton>{
  bool _hasBeenPressed = true;
 
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: widget.size!.width*0.3,
      onPressed: (){
        setState(() {
             _hasBeenPressed = !_hasBeenPressed;
        });
        widget.onPressed!();
      },
      color:_hasBeenPressed ? WHITE_COLOR :SECONDARY_COLOR,
      padding: EdgeInsets.all(0.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(MINI_BUTTON_BORDER_RADIUS),
          side: BorderSide(color: Colors.grey.shade400)
      ),
      child: Text(
        widget.buttonText,
        style: Theme.of(context)
            .textTheme
            .caption!
            .copyWith(color:_hasBeenPressed ? PRIMARY_TEXT_COLOR_BLACK : PRIMARY_TEXT_COLOR),
        textAlign: TextAlign.center,
      ),
    );
  }

}




