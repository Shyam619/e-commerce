import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/material.dart';

class MiniPaddingSpaceVertical extends StatelessWidget {
  MiniPaddingSpaceVertical();
  @override
  Widget build(BuildContext context) {
    return SizedBox(height: DEFAULT_PADDING_MIN);  }
}
