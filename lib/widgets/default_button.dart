
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {

  DefaultButton({this.buttonText, this.onPressed, this.size});

  final String? buttonText;
  final VoidCallback? onPressed;
  final Size? size;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: size!.width,
      onPressed: () => onPressed!(),
      color: ACCENT_COLOR,
      splashColor: WHITE_COLOR,
      padding: EdgeInsets.all(DEFAULT_BUTTON_PADDING),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(DEFAULT_BUTTON_RADIUS),
      ),
      child: Text(
        buttonText!,
        style: Theme.of(context)
            .textTheme
            .subtitle1!
            .copyWith(color: WHITE_COLOR),
        textAlign: TextAlign.center,
      ),
    );
  }
}
