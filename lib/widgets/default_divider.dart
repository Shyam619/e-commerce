import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/material.dart';

class DefaultDivider extends StatelessWidget {
  DefaultDivider();
  @override
  Widget build(BuildContext context) {
    return  Divider(
      thickness: DEFAULT_DIVIDER_THICKNESS,
      color: SHADOW_COLOR_LITE,
    );
  }
}
