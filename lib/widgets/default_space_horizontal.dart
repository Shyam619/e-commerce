import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/material.dart';

class DefaultSpaceHorizontal extends StatelessWidget {
  DefaultSpaceHorizontal();
  @override
  Widget build(BuildContext context) {
    return SizedBox(width: DEFAULT_SPACING);
  }
}
