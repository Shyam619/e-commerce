
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/material.dart';

class DefaultSpaceVertical extends StatelessWidget {
  DefaultSpaceVertical();
  @override
  Widget build(BuildContext context) {
    return SizedBox(height: DEFAULT_SPACING);
  }
}
