
class Constants {

  static const String APP_VERSION = "1.0.0";
  static const String VERSION_TEXT = "version ";

  ///APP VERSION, ITS IS REFERENCE FOR IS_UPDATE_AVAILABLE SERVICE
  static const String BUILD_VERSION = "1";
  static const String SERVICE_UPDATE_VERSION = "1.0";

  static const String APP_STORE_URL = "";
  static const String PLAY_STORE_URL = "";
  static const String PRIVACY_TERMS_URL = "";
  static  String APP_PLATFORM = "";
  static const String FONT_NAME_QUICK_SAND = "Quicksand";



  static const String BASE_URL = 'https://sobha.augray.com/api/';


  //font
  static const String FONT_ChronicleDisp = 'ChronicleDisp';
  static const String FONT_Ringside = 'Ringside';



//routes
  static const String LOGIN_PAGE_ROUTE = '/loginPage';
  static const String ONBOARD_MESSAGE_PAGE_ROUTE = '/onboardMessage';
  static const String CHANGE_PASSWORD_PAGE_ROUTE = '/changePassword';
  static const String CHOOSE_LANGUAGE_PAGE_ROUTE = '/changeLanguage';
  static const String GET_STARTED_PAGE_ROUTE = '/getStarted';
  static const String HOME_PAGE_ROUTE = '/homePage';

  //states
  static const String STATE_SUCCESS = 'SUCCESS';
  static const String STATE_FAILURE = 'FAILURE';
  static const String STATE_LOADING = 'LOADING';

  // SP keys
  static const String USER_ID_KEY = 'USER_ID';
  static const String USER_NAME_KEY = 'USER_NAME';
  static const String LOGIN_STATUS_KEY ="IS_LOGIN_ACTIVE";
  static const String LANGUAGE_KEY="Language";


  //common
  static const String MONTSERRAT = 'Montserrat';
  static const String DUMMY_DASH = "--";



  //Title
  static const String HOME_PAGE_TITLE = "";
  static const String BACK_DROP_TITLE = "";


  //Error Text
  static const String NO_INTERNET = "No Internet Connection";
  static const String INVALID_USER_CREDENTIALS = "Invalid User Credentials";
  static const String SOMETHING_WENT_WRONG = "something went wrong!!!";
  static const String SERVICE_IN_PROGRESS = "Service in progress";

  //Get started


  //LoginPage
  static const String EMP_ID_TEXT_INPUT_HINT = "Employee ID";
  static const String EMP_ID_TEXT_INPUT_ERROR = "Please enter valid Employee ID";
  static const String PASSWORD_TEXT_INPUT_HINT = "Password";
  static const String PASSWORD_TEXT_INPUT_ERROR = "Password Cannot be Empty";
  static const String LOGIN_PAGE_CHANGE_PASSWORD_MESSAGE = "PLEASE CONTACT IT ADMIN";
  static const String LOGIN_BUTTON_TEXT_GO = "GO";



  static const String LOGIN_SUB_HEADING_1 = "Log in for best experience";
  static const String LOGIN_SUB_HEADING_2 = "Experience the all new e-Com";
  static const String LOGIN_BUTTON_TEXT = "SIGN IN";
  static const String LOGIN_FORGOT_PASSWORD = "Forgot Password";
  static const String TERMS_TEXT = "By logging in, you are agreeing to the";
  static const String AND_TEXT = " and ";
  static const String TERMS_TEXT_LINK = " Terms of Service" ;
  static const String PRIVACY_TEXT_LINK = " Privacy Policy " ;
  static const String LOGIN_PAGE_EMAIL_ID_ERROR = "Please enter valid Mail ID";
  static const String LOGIN_PAGE_PHONE_NUMBER_ERROR = "Please enter Phone Number";
  static const String LOGIN_PAGE_EMAIL_ID_HINT = "Email ID";
  static const String LOGIN_PAGE_PHONE_NUMBER_HINT= "Phone Number";




  //Change password page
  static const String CHANGE_PASSWORD_SUB_HEADING_1 = "Change Password";
  static const String CHANGE_PASSWORD_SUB_HEADING_2 = "Enter your new password";
  static const String CHANGE_PASSWORD_BUTTON_TEXT = "SUBMIT";
  static const String CHANGE_PASSWORD_BOTTOM_INFO= "For More Information. Contact Admin";

  static const String CHANGE_PASSWORD_SUCCESS_MSG = "Password Updated Successfully";

  static const String NEW_PASSWORD_TEXT_INPUT_HINT = "New Password";
  static const String CONFIRM_PASSWORD_TEXT_INPUT_HINT = "Confirm Password";
  static const String BACK_TO_TEXT = "Back to ";
  static const String LOGIN_TEXT = "Login";
  static const String PASSWORD_NOT_MATCH_TEXT_INPUT_ERROR = "Password does not match";



  //Account
  static const String USER_IMAGE_URL_ACCOUNT_PAGE = "https://i.dlpng.com/static/png/6728153_preview.png";


  // Edit profile
  static const String EDIT_PROFILE_PAGE_TITLE = "Edit Profile";
  static const String EDIT_PROFILE_PAGE_USER_NAME_HINT = "User Name";
  static const String EDIT_PROFILE_PAGE_USER_NAME_ERROR = "Please enter valid User Name";
  static const String EDIT_PROFILE_PAGE_EMAIL_ID_HINT = "Mail ID";
  static const String EDIT_PROFILE_PAGE_EMAIL_ID_ERROR = "Please enter valid Mail ID";
  static const String EDIT_PROFILE_PAGE_MOBILE_NUMBER_HINT = "Mobile number";
  static const String EDIT_PROFILE_PAGE_MOBILE_NUMBER_ERROR = "Please enter valid Mobile Number";
  static const String EDIT_PROFILE_PAGE_DOB_HINT = "Date of Birth";
  static const String EDIT_PROFILE_PAGE_DOB_ERROR = "Please enter valid Date";



  static const String ADD_ADDRESS_PAGE_FULL_NAME_HINT = "Full name";
  static const String ADD_ADDRESS_PAGE_FULL_NAME_ERROR = "Please enter valid name";

  static const String ADD_ADDRESS_PAGE_MOBILE_NUMBER_HINT = "Mobile number";
  static const String ADD_ADDRESS_PAGE_MOBILE_NUMBER_ERROR = "Please enter valid Mobile Number";

  static const String ADD_ADDRESS_PAGE_PIN_CODE_HINT = "Pincode";
  static const String ADD_ADDRESS_PAGE_PIN_CODE_ERROR = "Please enter valid Pin code";

  static const String ADD_ADDRESS_PAGE_BUILDING_NUMBER_HINT = "Flat, House no., Building, Company, Apartment";
  static const String ADD_ADDRESS_PAGE_BUILDING_NUMBER_ERROR = "Please enter valid details";

  static const String ADD_ADDRESS_PAGE_AREA_HINT = "Area, Street, Sector, Village";
  static const String ADD_ADDRESS_PAGE_AREA_ERROR = "Please enter valid details";

  static const String ADD_ADDRESS_PAGE_LANDMARK_HINT = "Landmark";
  static const String ADD_ADDRESS_PAGE_LANDMARK_ERROR = "Please enter valid details";

  static const String ADD_ADDRESS_PAGE_CITY_HINT = "Town/City";
  static const String ADD_ADDRESS_PAGE_CITY_ERROR = "Please enter valid details";

  static const String ADD_ADDRESS_PAGE_STATE_HINT = "State";
  static const String ADD_ADDRESS_PAGE_STATE_ERROR = "Please enter valid details";




  static const String EDIT_PROFILE_MALE = " Male ";
  static const String EDIT_PROFILE_FEMALE = "Female";
  static const String EDIT_PROFILE_ABOUT_ME_TEXT = "About Me :";


  static const String EDIT_PROFILE_PAGE_BUTTON_TEXT = "Save Changes";


  ///homepage
  static const String SHOP_PAGE_HEADING = "SHOP";
  static const String CATEGORY_PAGE_HEADING = "CATEGORY";
  static const String CART_PAGE_HEADING = "CART";
  static const String ACCOUNT_PAGE_HEADING = "MY ACCOUNT";

  ///Dashboard
  static const String POPULAR_MENU_TEXT1 = "Popular";
  static const String POPULAR_MENU_TEXT2 = "Flash Sell";
  static const String POPULAR_MENU_TEXT3 = "Electronics";
  static const String POPULAR_MENU_TEXT4 = "Personal Care";
  static const String CATEGORY_HEADING_TEXT = "Category";
  static const String MORE_CATEGORY_HEADING_TEXT = "more >>";
  static const String OFFER_TEXT = "40% to 50%";
  static const String TOP_CATEGORY_HEADING_TEXT = "TOP CATEGORIES FOR YOU";

  ///Cart
  static const String CART_SUB_TOTAL_TEXT = "Sub Total";
  static const String CART_SHIPPING_TEXT = "Shipping";
  static const String CART_BAG_TOTAL_TEXT = "Bag Total";
  static const String CART_BUTTON_TEXT = "Continue";

  ///Product List Page
  static const String SEARCH_VIEW_HINT_TEXT = "Search Products...";
  static const String CATEGORY_TEXT = "Category's";
  static const String ADD_CART_BUTTON_TEXT ="Add Card";
  static const String BAY_NOW_BUTTON_TEXT ="Buy Now";
  static const String BUTTON_SPACE_TEXT = "or";

  ///Product Details
  static const String PRODUCT_DETAIL_SUB_HEADING = "Available Size";
  static const String COLOUR_AND_SIZE_HEADING_TEXT = "COLOUR";

}


