import 'package:flutter/material.dart';

const PRIMARY_COLOR = const Color(0xFF18191e);
const SECONDARY_COLOR = const Color(0xFFd5b369);
const ACCENT_COLOR = const Color(0xff1ba0f8);
const SECONDARY_COLOR_SHADE_DARK = const Color(0xFFbe9b77);
const SECONDARY_COLOR_SHADE_LITE = const Color(0xFFd0bb88);
const WHITE_COLOR = Colors.white;
const SECONDARY_WHITE_COLOR = Colors.white70;
const BACKGROUND_COLOR = const Color(0xFFFFFFFF);


///TEXT COLOR
const PRIMARY_TEXT_COLOR = const Color(0xFFFFFFFF);
const PRIMARY_TEXT_COLOR_BLACK = const Color(0xFF18191e);
const PRIMARY_TEXT_FILED_BORDER_COLOR = const Color(0xFFFFFFFF);
const TEXT_COLOR_ASH = const Color(0xFF99ffffff);
const TEXT_COLOR_ASH_LITE = const Color(0xFF999999);




const ERROR_COLOR = Colors.redAccent;
const SUCCESS_COLOR = Colors.green;


const SHADOW_COLOR_DARK = const Color(0xFF606060);
const SHADOW_COLOR = const Color(0xFF797979);
const SHADOW_COLOR_LITE = const Color(0xFF727272);

const kTextColor = Color(0xFF535353);
const kTextLightColor = Color(0xFFACACAC);
