import 'dart:core';
import 'api_model/login_response.dart';
import 'api_provider_client.dart';


class Repository {
  final apiProvider = ApiProviderClient();
  Future<LoginResponse> checkLogin(String empId, String password, String token) => apiProvider.checkUserLoginDetails(empId, password ,token);



}