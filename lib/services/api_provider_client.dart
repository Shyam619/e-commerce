import 'dart:async';
import 'dart:convert';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:http/http.dart' show Client, Response;
import 'api_exception.dart';
import 'api_model/login_response.dart';


class ApiProviderClient {

  final checkUserLoginPath = "technicians/login";

  Client client = Client();

  dynamic _handleResponse(Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occurred while Communication with Server with StatusCode: ${response.statusCode}');
    }
  }


  /// CHECK LOGIN CREDENTIALS
  Future<LoginResponse> checkUserLoginDetails(String empId, String password, String token) async {

    Map<String, dynamic> params = new Map();
    params["emp_id"] = empId;
    params["password"] = password;
    params["device_token"] = token;

    print(params.toString());
    var url = Uri.parse(Constants.BASE_URL + checkUserLoginPath,);

    final response = await client.post(url,
        headers: {'Content-type': 'application/json'},
        body: json.encoder.convert(params));
    var responseJson = _handleResponse(response);
    print(response.body.toString());
    return LoginResponse.fromJson(responseJson);
  }

}




