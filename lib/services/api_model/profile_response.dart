import 'package:ecommerce_flutter_app/models/user_profile_detail.dart';

class ProfileResponse {

  ProfileResponse({
    this.status,
    this.message,
    this.data,

});
  bool? status;
  String? message;
UserProfileDetail?   data;

  factory ProfileResponse.fromJson(Map<String, dynamic> json) => ProfileResponse(
    status: json["status"],
    message: json["message"],
    data: UserProfileDetail.fromJson(json["data"]),
  );
  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data!.toJson(),
  };
}