
import 'package:ecommerce_flutter_app/models/user_details.dart';

class LoginResponse {
  LoginResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  UserDetails? data;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    status: json["status"],
    message: json["message"],
    data: UserDetails.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data!.toJson(),
  };

}


