import 'dart:io';
import 'package:ecommerce_flutter_app/pages/splash/splash_page.dart';
import 'common/constants.dart';
import 'common/shared_pref_util.dart';
import 'package:flutter/material.dart';
import 'common/dimensions.dart';
import 'common/colors.dart';

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ECommerce',
      theme: _buildTheme(),
      initialRoute: '/',
      routes: {
        '/': (context) => SplashPage(),
       // Constants.LOGIN_PAGE_ROUTE:(context) =>  LoginPage(),
        //Constants.HOME_PAGE_ROUTE:(context) =>  HomePage(),
      },
    );
  }
}

//this class is for approve ssl certificate issue
class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}


ThemeData _buildTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    primaryColor: PRIMARY_COLOR,
    accentColor: ACCENT_COLOR,
    backgroundColor: BACKGROUND_COLOR,
    appBarTheme: _appBarTheme(base.appBarTheme),
    textTheme: _textTheme(base.textTheme),
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: ACCENT_COLOR,
    ),
  );
}

AppBarTheme _appBarTheme(AppBarTheme base) => base.copyWith(
    color: BACKGROUND_COLOR,
    brightness: Brightness.light,
    textTheme: TextTheme(
        headline6: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 18.0,
            fontFamily: Constants.FONT_NAME_QUICK_SAND,
            color: ACCENT_COLOR)),
    iconTheme: IconThemeData(color: ACCENT_COLOR));

TextTheme _textTheme(TextTheme base) {
  return base.copyWith(

    headline1: base.headline1!.copyWith(
        fontWeight: FontWeight.w500,
        fontFamily: Constants.FONT_NAME_QUICK_SAND,
        color: PRIMARY_TEXT_COLOR),

    subtitle1: base.subtitle1!.copyWith(
      //fontSize: TITLE_FONT_SIZE,
          fontWeight: FontWeight.w600,
        fontFamily: Constants.FONT_NAME_QUICK_SAND,
        color: PRIMARY_TEXT_COLOR),

    subtitle2: base.subtitle2!.copyWith(
      //fontSize: TITLE_FONT_SIZE,
         fontWeight: FontWeight.w600,
        fontFamily: Constants.FONT_NAME_QUICK_SAND,
        color: PRIMARY_TEXT_COLOR),

    caption: base.caption!.copyWith(
        fontWeight: FontWeight.w400,
        fontFamily: Constants.FONT_NAME_QUICK_SAND,
        //  fontSize: TEXT_FONT_SIZE,
        color: PRIMARY_TEXT_COLOR),

    bodyText1: base.bodyText1!.copyWith(
        fontFamily: Constants.FONT_NAME_QUICK_SAND,
        //   fontSize: TEXT_FONT_SIZE,
        color: PRIMARY_TEXT_COLOR),

    bodyText2: base.bodyText2!.copyWith(
        fontFamily: Constants.FONT_NAME_QUICK_SAND,
        //  fontSize: TEXT_LARGE_FONT_SIZE,
        color: PRIMARY_TEXT_COLOR),

    button: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w500,
        fontFamily: Constants.FONT_NAME_QUICK_SAND,
        fontSize: BUTTON_TEXT_LARGE_FONT_SIZE),
  );
}



