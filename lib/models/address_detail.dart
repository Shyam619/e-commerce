class AddressBook {

  String? id;
  String? state;
  String? userName;
  String? mobileNumber;
  String? pinCode;
  String? role;
  String? address1;
  String? address2;
  String? landmark;
  String? cityTown;
  String? country;

  AddressBook({
    this.id,
    this.state,
    this.userName,
    this.mobileNumber,
    this.pinCode,
    this.role,
    this.address1,
    this.address2,
    this.landmark,
    this.cityTown,
    this.country,
  });

  factory AddressBook.formJson(Map<String, dynamic> json) => AddressBook(
    id: json["id"],
    state: json["state"],
    userName: json["user_name"],
    mobileNumber: json["mobile_number"],
    pinCode: json["pincode"],
    role: json["role"],
    address1: json['address1'],
    address2: json['address2'],
    landmark: json['landmark'],
    cityTown: json['city_town'],
    country: json["country"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "state": state,
    "user_name": userName,
    "mobile_number": mobileNumber,
    "pincode": pinCode,
    "role": role,
    "address1": address1,
    "address2": address2,
    "landmark": landmark,
    "city_town": cityTown,
    "country": country,
  };

  static fromJson(json) {}


}