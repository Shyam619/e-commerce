class UserDetails {

  int? id;
  String? userImage;
  String? userName;
  String? emailId;
  String? employeeId;
  String? role;
  DateTime? dateOfJoining;
  DateTime? lastLoggedTime;
  String? status;
  String? assetId;
  String? otp;

  UserDetails({
    this.id,
    this.userImage,
    this.userName,
    this.emailId,
    this.employeeId,
    this.role,
    this.dateOfJoining,
    this.lastLoggedTime,
    this.status,
    this.assetId,
    this.otp,
  });

  factory UserDetails.formJson(Map<String, dynamic> json) => UserDetails(
    id: json["id"],
    userImage: json["image"],
    userName: json["user_name"],
    emailId: json["email_id"],
    employeeId: json["employee_id"],
    role: json["role"],
    dateOfJoining: json['date_of_joining'] == null ? null : DateTime.parse(json['date_of_joining']),
    lastLoggedTime: json['last_logged_time'] == null ? null : DateTime.parse(json['last_logged_time']),
    status: json['status'],
    assetId: json['asset_id'],
    otp: json["otp"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "image": userImage,
    "user_name": userName,
    "email_id": emailId,
    "employee_id": employeeId,
    "role": role,
    "date_of_joining": dateOfJoining!.toIso8601String(),
    "last_logged_time": lastLoggedTime!.toIso8601String(),
    "status": status,
    "asset_id": assetId,
    "otp": otp,
  };

  static fromJson(json) {}


}