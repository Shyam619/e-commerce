

import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/pages/home/home_page.dart';
import 'package:ecommerce_flutter_app/widgets/default_button.dart';
import 'package:flutter/material.dart';

class OtpPage extends StatefulWidget {
  const OtpPage({Key? key}) : super(key: key);

  @override
  _OtpPageState createState() => _OtpPageState();
}



class _OtpPageState extends State<OtpPage> {
 var size;

  void onBackPressed() {
    Navigator.of(context).pop();
  }

 void gotoNextPage(){
    Navigator.pushReplacement(
     context,
     MaterialPageRoute(builder: (context) => HomePage()),
   );
 }


  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
       body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(DEFAULT_PADDING),
          child: ListView(
            children: [
              backIcon(),
              pageHeadingText(),
              SizedBox(height: DEFAULT_PADDING_MIN),
              enterOtpHeadingText(),
              SizedBox(height: DEFAULT_PADDING),
              SizedBox(height: DEFAULT_PADDING),
              otpEnterTextFieldContainers(),
              SizedBox(height: DEFAULT_PADDING),
               didNotYetReceiveAnyCode(),
              SizedBox(height: DEFAULT_PADDING),
              resendNewCode(),
            ],
          ),
        ),
      ),
    );
  }

  Widget backIcon()=> Align(
    alignment: Alignment.topLeft,
    child: GestureDetector(
      onTap: () => onBackPressed(),
      child: Icon(
        Icons.arrow_back,
        size: 25,
        color: Colors.black54,
      ),
    ),
  );

  Widget pageHeadingText() => Center(
    child: Text(
      'Verification',
      style: Theme.of(context)
          .textTheme
          .headline5!.copyWith(fontWeight: FontWeight.w500, color:PRIMARY_TEXT_COLOR_BLACK),
    ),
  );

  Widget enterOtpHeadingText() => Center(
    child: Text(
      "Enter your OTP code number",
      style: Theme.of(context)
          .textTheme
          .subtitle2!.copyWith(fontWeight: FontWeight.bold,color: Colors.black38),
    ),
  );


  Widget otpEnterTextFieldContainers() =>  Container(
    padding: EdgeInsets.all(5),
    decoration: BoxDecoration(
      color: WHITE_COLOR,
      borderRadius: BorderRadius.circular(DEFAULT_BORDER_RADIUS),
    ),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _textFieldOTP(first: true, last: false),
            _textFieldOTP(first: false, last: false),
            _textFieldOTP(first: false, last: false),
            _textFieldOTP(first: false, last: true),
          ],
        ),
        SizedBox(height: DEFAULT_PADDING_MIN),
        SizedBox(height: DEFAULT_PADDING),
        SizedBox(
          width: double.infinity,
          child: ElevatedButton(
            onPressed: () {
              gotoNextPage();
            },
            style: ButtonStyle(
              foregroundColor:
              MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor:
              MaterialStateProperty.all<Color>(ACCENT_COLOR),
              shape:
              MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(DEFAULT_BORDER_RADIUS),
                ),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(14.0),
              child: Text(
                'Verify',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),
        )
      ],
    ),
  );

  Widget verifyButton() => DefaultButton(
    onPressed: (){
      gotoNextPage();
    },
    size: size,
    buttonText: "Verify",
  );

  Widget didNotYetReceiveAnyCode() =>  Text(
    "Didn't you receive any code?",
    style: TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      color: Colors.black38,
    ),
    textAlign: TextAlign.center,
  );

 Widget resendNewCode() =>Text(
   "Resend New Code",
   style: TextStyle(
     fontSize: 18,
     fontWeight: FontWeight.bold,
     color: ACCENT_COLOR,
   ),
   textAlign: TextAlign.center,
 );

 Widget resendNewCode1() =>Text(
   "Resend New Code",
   style:  Theme.of(context)
     .textTheme
     .subtitle2!.copyWith(fontWeight: FontWeight.bold,color: Colors.black38),

 );



     Widget _textFieldOTP({required bool first, last}) {
    return Container(
      height: 70,
      child: AspectRatio(
        aspectRatio: 1.0,
        child: TextField(
          autofocus: true,
          onChanged: (value) {
            if (value.length == 1 && last == false) {
              FocusScope.of(context).nextFocus();
            }
            if (value.length == 0 && first == false) {
              FocusScope.of(context).previousFocus();
            }
          },
          showCursor: false,
          readOnly: false,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold ,color: Colors.black87),
          keyboardType: TextInputType.number,
          maxLength: 1,
          decoration: InputDecoration(
            counter: Offstage(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: Colors.black12),
                borderRadius: BorderRadius.circular(12)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: ACCENT_COLOR),
                borderRadius: BorderRadius.circular(12)),
          ),
        ),
      ),
    );
  }
}
