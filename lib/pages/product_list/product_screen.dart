
import 'package:flutter/material.dart';

import 'components/product_main.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      // appBar: buildAppBar(),
      body: ProductMain(),
    );
  }

  AppBar buildAppBar() {
   return AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        //iconTheme: IconThemeData(color: Colors.green),
        actionsIconTheme: IconThemeData(color: Colors.black54),
        leading: Icon(Icons.arrow_back_outlined,color: Colors.black54,),
        actions: [
          Row(
            children: [
              IconButton(icon: Icon(Icons.notification_important_outlined),
                onPressed: () {
                },),
              IconButton(icon: Icon(Icons.shopping_cart),
                onPressed: () {
                },),],
          )
        ]
    );
  }
}
