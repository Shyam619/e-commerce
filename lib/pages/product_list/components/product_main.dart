import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/pages/product_detail_one/detail_screen.dart';
import 'package:ecommerce_flutter_app/theme/light_colour.dart';
import 'package:ecommerce_flutter_app/theme/theme.dart';
import 'package:flutter/material.dart';
import 'categorries.dart';
import 'item_card.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';


class ProductMain extends StatelessWidget {
  late final Product product;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _search(),
        Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Text(
            "Women",
            style: Theme.of(context)
                .textTheme
                .headline5!
                .copyWith(fontWeight: FontWeight.bold),),
        ),
        Categories(),
        Expanded(
          child: Padding(
            padding:  EdgeInsets.symmetric(horizontal: kDefaultPaddin),
            child: GridView.builder(
                itemCount: products.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: kDefaultPaddin,
                  crossAxisSpacing: kDefaultPaddin,
                  childAspectRatio: 0.75,
                ),
                itemBuilder: (context, index) => ItemCard(
                      product: products[index],
                      press: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailScreen(
                              product: products[index],
                            ),
                          )),
                    )),
          ),
        ),
      ],
    );
  }
  Widget _search(){
    return Container(
      margin: AppTheme.padding,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              height: SMALL_CONTAINER_HEIGHT,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: LightColor.lightGrey.withAlpha(150),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: Constants.SEARCH_VIEW_HINT_TEXT,
                    hintStyle: TextStyle(fontSize: TEXT_STYLE_FONT_SIZE_TO_SMALL),
                    contentPadding: EdgeInsets.only(left: 10, right: 10, bottom: 0, top: 5),
                    prefixIcon: Icon(Icons.search, color: Colors.black54),
                ),

              ),
            ),
          ),
          SizedBox(width: SEIZED_BOX_WIDTH_SMALL),
          Container(
            height: 37.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
            child: ElevatedButton(
                onPressed: () {},
              style: ElevatedButton.styleFrom(
                  elevation:0.4,
                  primary:Colors.white54,
                minimumSize: Size(2.0, 2.0)
              ),
              child:  IconButton(icon: Icon(
                Icons.filter_list_outlined,
                color:Colors.black54,),
               onPressed: () {
    },)),
          )
        ],
      ),
    );
  }
}
