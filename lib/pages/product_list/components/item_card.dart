import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:flutter/material.dart';


class ItemCard extends StatelessWidget {
  final Product product;
  final VoidCallback press;
  const ItemCard({
    Key? key,
    required this.product,
    required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(kDefaultPaddin),
              decoration: BoxDecoration(
                color: product.color,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Hero(
                tag: "${product.id}",
                child: Image.asset(product.image),
              ),
            ),
          ),
          Padding(
            padding:EdgeInsets.symmetric(vertical: kDefaultPaddin / 4),
            child: Text(
              // products is out demo list
              product.title,
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, fontWeight: FontWeight.w500,),

              /*style: TextStyle(fontWeight: FontWeight.bold,color: TEXT_COLOR_ASH_LITE),*/
            ),
          ),
          Text(
            "\$${product.price}",
            style: Theme.of(context)
                .textTheme
                .bodyText2!
                .copyWith(color: TEXT_COLOR_ASH_LITE,fontWeight: FontWeight.w400,),

            /*style: TextStyle(fontWeight: FontWeight.bold, color: TEXT_COLOR_ASH_LITE),*/
          )
        ],
      ),
    );
  }
}
