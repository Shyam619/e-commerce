import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:ecommerce_flutter_app/pages/payment/payment_page.dart';
import 'package:ecommerce_flutter_app/widgets/default_space_vertical.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class CartPage extends StatefulWidget {

  Product product;
  int quantity;
  CartPage({Key? key,
    required this.product,
    required this.quantity,
  }) : super(key: key);

  @override
  _CartPageState createState() {
    return _CartPageState();

  }
}

class _CartPageState extends State<CartPage> {
  var size;
  int quantity = 0;
  List<String> shoes =["Knee high boots",
    "Insulated boots",
    "Boots","Chuck Taylor",
    "Golf shoes",
    "Hiking boots","Lita","Stilettos","Boat shoes","Stilettos",
    "Wedges",
    "Cone heels",
    "High heels",];
  late Product product;
  int subTotal = 0;
  int bagTotal =0;

  void initialCartAmount() {
    setState(() {
      subTotal = quantity*product.price;
      bagTotal = subTotal + 60;

    });


  }

  @override
  void initState() {
    super.initState();
    product =widget.product;
    quantity =widget.quantity;
    initialCartAmount();

  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget deliveryAddress() {
    Widget deliveryIcon() {
      return Icon(
        Icons.add_location_alt_outlined,
        color: PRIMARY_COLOR,
        size: 15,
      );
    }

    var deliveryText = Text(
      " Delivery to naveen",
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.caption!.copyWith(
        fontWeight: FontWeight.w400,
        color: Colors.black87,
      ),
    );

    return Container(
      color: Colors.grey[100],
      padding: EdgeInsets.all(10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          deliveryIcon(),
          /*deliveryText(),*/
          deliveryText
        ],
      ),
    );
  }

  Widget proceedButton() {
    return MaterialButton(
      minWidth: size.width,
      onPressed: () {
        /*print("button clicked");*/
        /*showDialog(
            context: context,
            builder: (context) => requestSuccessfulPopUpPopUp(context));*/

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => PaymentPage(totalAmount: 600)),
        );

      },
      padding: EdgeInsets.all(0.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(DEFAULT_BORDER_RADIUS),
      ),
      child: Ink(
        width: size.width,
        decoration: BoxDecoration(
          color: ACCENT_COLOR,
          borderRadius: BorderRadius.circular(DEFAULT_BORDER_RADIUS),
        ),
        child: Padding(
            padding:
            EdgeInsets.only(left: 20, right: 20, top: 12, bottom: 12),
            child: Text(
              Constants.CART_BUTTON_TEXT,
              style: Theme.of(context).textTheme.button!.copyWith(color: SECONDARY_COLOR),
              textAlign: TextAlign.center,
            )),
      ),
    );
  }

  Widget cartProducts() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 1,
      itemBuilder: (context, index) {
        return cartCard(product);

          /*CartCard(singleProductData: product);*/
      },
    );
  }

  Widget fairDetails() {
    Widget subTotalRate() {
      var subText = Text(
        Constants.CART_SUB_TOTAL_TEXT,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
          fontWeight: FontWeight.w600,
          color:PRIMARY_COLOR,
        ),
      );

      var subValue = Text(
        subTotal.toString(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
          fontWeight: FontWeight.w600,
          color: PRIMARY_COLOR,
        ),
      );

      return Row(
        children: [subText, Spacer(), subValue],
      );
    }

    Widget shippingRate() {
      var subText = Text(
        Constants.CART_SHIPPING_TEXT,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
          fontWeight: FontWeight.w600,
          color: PRIMARY_COLOR,
        ),
      );

      var subValue = Text(
        "60",
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
          fontWeight: FontWeight.w600,
          color: PRIMARY_COLOR,
        ),
      );

      return Row(
        children: [subText, Spacer(), subValue],
      );
    }

    Widget bagTotalRate() {
      var subText = Text(
        Constants.CART_BAG_TOTAL_TEXT,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
          fontWeight: FontWeight.w600,
          color: PRIMARY_COLOR,
        ),
      );

      var subValue = Text(
        bagTotal.toString(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
          fontWeight: FontWeight.w600,
          color:PRIMARY_COLOR,
        ),
      );

      return Row(
        children: [
          subText,
          Spacer(),
          subValue,
        ],
      );
    }

    return Container(
      margin: EdgeInsets.symmetric(vertical: CART_MARGIN_HORIZONTAL),
      padding: EdgeInsets.all(20.0),
      height: size.height * 0.2,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.blueGrey[100],
          borderRadius: BorderRadius.all(Radius.circular(16))),
      child: Column(
        children: [
          Expanded(child: subTotalRate()),
          Divider(
            color: Colors.white,
            thickness: CART_DIVIDER_THICKNESS,
          ),
          Expanded(child: shippingRate()),
          Divider(
            color: Colors.white,
            thickness: CART_DIVIDER_THICKNESS,
          ),
          Expanded(child: bagTotalRate())
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        _sendDataBack(context);
        return true;
      },
      child: Scaffold(
        body: Container(
          color: Colors.blueGrey[50],
          child: Stack(
            children: [
              Container(
                height: size.height*0.55,
                 margin: EdgeInsets.symmetric(),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    deliveryAddress(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: CART_MARGIN_MIN_HORIZONTAL),
                      child: cartProducts(),
                    ),

                    /*Divider(
                      color: Colors.grey[400],
                      thickness: CART_DIVIDER_MAX_THICKNESS,
                    ),*/

                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 100),
                    width: double.infinity,
                    child: fairDetails()),
              ),
              Align(
                alignment: Alignment.bottomCenter,

                child: Container(

                    padding: EdgeInsets.only(left: 20, right: 20, bottom: 60),
                    width: double.infinity,
                    child: proceedButton()),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget cartCard(Product singleProductData) {

    void calculationPlus() {
      setState(() {

        subTotal += singleProductData.price;
        subTotal ==0 ? bagTotal = 0 : bagTotal = subTotal + 60;
      });

    }
    void calculationMinus() {
      setState(() {

        subTotal -= singleProductData.price;
        subTotal ==0 ? bagTotal = 0 : bagTotal = subTotal + 60;
      });

    }

    productImage() => CachedNetworkImage(
      alignment: Alignment.center,
      imageUrl:
      "https://www.pngall.com/wp-content/uploads/4/Leather-Bag-PNG.png",
      imageBuilder: (context, imageProvider) => Container(
          width: double.infinity,
          height: DASHBOARD_CARD_PRODUCT_SIZE,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS),
                topRight:
                Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS)),
            image: DecorationImage(
              image: NetworkImage(
                  "https://www.pngall.com/wp-content/uploads/4/Leather-Bag-PNG.png"),
              fit: BoxFit.cover,
            ),
          )),
      placeholder: (context, url) =>
          Center(child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
    productName() => Text(
      singleProductData.title,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.headline6!.copyWith(
        fontWeight: FontWeight.w400,
        color: PRIMARY_TEXT_COLOR_BLACK,
      ),
    );
    productDesc() => Text(
      singleProductData.description,
      textAlign: TextAlign.left,
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
        fontWeight: FontWeight.w500,
        color: TEXT_COLOR_ASH_LITE,
      ),
    );
    productPrice() => Text(
      "Price: "+singleProductData.price.toString(),
      textAlign: TextAlign.left,
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
        fontWeight: FontWeight.w600,
        color: Colors.black,
      ),
    );
    Widget incDec() {
      Widget _incrementButton(int index) {
        return Container(
          height: CART_INC_DEC_SIZE,
          width: CART_INC_DEC_SIZE,
          child: FloatingActionButton(
              elevation: 0.5,

              onPressed: () {
                setState(() {
                  quantity++;
                  calculationPlus();

                });
              },
              child: Icon(Icons.add, color: PRIMARY_COLOR),
              backgroundColor: WHITE_COLOR),
        );
      }

      Widget _decrementButton(int index) {
        return Container(
          height: CART_INC_DEC_SIZE,
          width: CART_INC_DEC_SIZE,
          child: FloatingActionButton(
              elevation: 0.5,
              onPressed: () {

                quantity==0 ? Container() : setState(() {
                  quantity--;
                  calculationMinus();

                });


              },
              child: new Icon(Icons.remove, color: PRIMARY_COLOR),
              backgroundColor: WHITE_COLOR),
        );
      }

      return Container(
        height: 40.0,
        width: 100.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _decrementButton(0),
            Text(
              quantity.toString(),
              style: TextStyle(fontSize: 18.0, color: PRIMARY_TEXT_COLOR_BLACK),
            ),
            _incrementButton(0),
          ],
        ),
      );
    }

    return Container(
      height: 180.0,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(DEFAULT_BORDER_RADIUS),
        ),
        child: InkWell(
          child: Row(
            children: [
              Expanded(flex: 1, child: productImage()),
              Expanded(
                flex: 3,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(child: productName()),
                      Expanded(child: productDesc()),
                      SizedBox(height: DEFAULT_SIZED_BOX_MIN_HEIGHT,),
                      Row(
                        children: [
                          incDec(),
                          Spacer(),
                          productPrice()
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );

  }

///Payment Pop up
  requestSuccessfulPopUpPopUp(BuildContext context) {
    Widget icon() =>
        Container(
          alignment: Alignment.topLeft,
          height: 40,
          width: 40,
          child: Image.asset("assets/ic_emoji.png"),
        );

    Widget HeadingText() =>
        Text(
          "Payment was Successfully paid!",
          style: Theme
              .of(context)
              .textTheme
              .headline6!
              .copyWith(
            color: PRIMARY_TEXT_COLOR_BLACK,
            fontWeight: FontWeight.w800,
          ),
        );

    Widget SubHeadingText() =>
        Text(
          "Be happy with the products",
          style: Theme
              .of(context)
              .textTheme
              .subtitle2!
              .copyWith(
            color: PRIMARY_TEXT_COLOR_BLACK,
            fontWeight: FontWeight.w400,

          ),
          textAlign: TextAlign.start,
        );

    Widget button() => Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: size.width * 1,
            height: size.height * 0.07,
            decoration: ShapeDecoration(
              color: ACCENT_COLOR,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(5.0),
                    bottomRight: Radius.circular(5.0)),
              ),
            ),
            child: new MaterialButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  /*Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );*/
                },
                child: Text(
                  "Okay",
                  style: Theme
                      .of(context)
                      .textTheme
                      .headline5!
                      .copyWith(
                    color: SECONDARY_COLOR,
                    fontFamily: Constants.FONT_NAME_QUICK_SAND,
                    fontWeight: FontWeight.w800,
                  ),
                )),
          ),
        );

    return Dialog(
      backgroundColor: WHITE_COLOR,
      elevation: DEFAULT_ELEVATION,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0)),
      child: Container(
        height: size.height * 0.4,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(32),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  DefaultSpaceVertical(),
                  icon(),
                  SizedBox(
                    height: DEFAULT_MIN_PADDING,
                  ),
                  HeadingText(),
                  SizedBox(
                    height: DEFAULT_MIN_PADDING,
                  ),
                  SubHeadingText(),

                ],
              ),
            ),
            Expanded(child: SizedBox()),
            button(),
          ],
        ),
      ),
    );
  }
  ///send data to previous page
  void _sendDataBack(BuildContext context) {
    Navigator.pop(context, quantity);
  }

}
