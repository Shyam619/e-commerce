
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';

class ProductTitleWithImage extends StatelessWidget {
  const ProductTitleWithImage({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: kDefaultPaddin),
      child:Column(
          crossAxisAlignment:CrossAxisAlignment.start,
          children: [
           Text(
            product.heading,
             style: TextStyle(color: Colors.black),
           ),
            Text(
              product.title,
              style: Theme.of(context)
                  .textTheme
                  .headline4!
                  .copyWith(color: Colors.black54, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: kDefaultPaddin),
            Row(
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(text: "Price\n"),
                      TextSpan(
                        text: "\$${product.price}",
                        style: Theme.of(context).textTheme.headline4!.
                        copyWith(
                            color: Colors.black54,
                            fontWeight:
                            FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: kDefaultPaddin),
                Expanded(
                  child: Hero(
                    tag: "${product.id}",
                    child: Image.asset(
                      product.image.toString(),
                      fit: BoxFit.fill,
                    ),
                  ),
                )
              ],
            )
          ],
        )
      );
    }
  }