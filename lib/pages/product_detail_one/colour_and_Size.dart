import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorAndSize extends StatelessWidget {

  const ColorAndSize({
     Key? key, required this.product,}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Row(
      children:<Widget> [
       Expanded(child:Column(
       crossAxisAlignment:CrossAxisAlignment.start,
         children: [
          Text(Constants.COLOUR_AND_SIZE_HEADING_TEXT, style:TextStyle(color:PRIMARY_TEXT_COLOR_BLACK)),
          Row(
            children: [
             ColorDot(
              color : Color(0xFF356C95),
               isSelected: true,
             ),
              ColorDot(color: Color(0xFFF8C078)),
              ColorDot(color: Color(0xFFA29B9B)),
            ],
          )
         ],
       )),
      ],
    );
  }
  
}

class ColorDot extends StatelessWidget {
  final Color color;
  final bool isSelected;
  const ColorDot({
     Key? key,
    required this.color,
    // by default isSelected is false
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: kDefaultPaddin / 4,
        right: kDefaultPaddin / 2,
      ),
      padding: EdgeInsets.all(2.5),
      height: PRODUCT_DETAIL_CONTAINER_HEIGHT_SMALL,
      width: PRODUCT_DETAIL_CONTAINER_WIDTH_TO_SMALL,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: isSelected ? color : PRIMARY_TEXT_COLOR_BLACK,
        ),
      ),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.rectangle,
        ),
      ),
    );
  }
}