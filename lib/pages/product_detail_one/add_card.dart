import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:ecommerce_flutter_app/pages/cart/cart_page.dart';
import 'package:flutter/material.dart';

class AddToCart extends StatelessWidget {
  final Product product;
  const AddToCart({
    Key? key,
    required this.product,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        //  Container(
        // margin: EdgeInsets.only(right:kDefaultPaddin ),
        //  height: 50.0,
        //  width: 50.0,
        //  decoration: BoxDecoration(
        //    borderRadius:BorderRadius.circular(18),
        //    border:Border.all(
        //     color:Colors.black26
        //    )
        //    ),
        //    // child: IconButton(
        //    //   icon: new Icon(Icons.shopping_cart),
        //    //   onPressed: () {  },
        //    //   color: Colors.black26,
        //    // ),
        //   ),
        // SizedBox(height: 20.0,),
        // Expanded(
        //       child: SizedBox(
        //       height: 50.0,
        //         child: FlatButton(
        //         shape: RoundedRectangleBorder(
        //         borderRadius: BorderRadius.circular(18)),
        //        color:Colors.black,
        //       onPressed: () {},
        //           child: Text(
        //            "Buy Now".toUpperCase(),
        //               style: TextStyle(
        //               fontSize: 17,
        //               fontWeight: FontWeight.bold,
        //               color: Colors.white,
        //               ),
        //           ),
        //       ))
        //   ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 15.0),
          child: Column(
            children: [
              SizedBox(
                height: PRODUCT_DETAIL_SEIZED_BOX_HEIGHT_BIG,
                width: 300.0,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18)),
                    color: Colors.black,
                    onPressed: () {},
                    child: Text(
                      Constants.BAY_NOW_BUTTON_TEXT.toUpperCase(),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                        color: PRIMARY_TEXT_COLOR,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 24.0, 0.0, 0.0),
                child: SizedBox(
                  height: PRODUCT_DETAIL_SEIZED_BOX_HEIGHT_BIG,
                  child: Text(Constants.BUTTON_SPACE_TEXT.toUpperCase(),
                      style: TextStyle(
                          color: PRIMARY_TEXT_COLOR_BLACK, fontSize: 17)),
                ),
              ),
              SizedBox(
                height: PRODUCT_DETAIL_SEIZED_BOX_HEIGHT_BIG,
                width: 100.0,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18)),
                    color: Colors.black,
                    onPressed: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) =>
                      //         CartPage(product: product),
                      //   ),
                      // );


                    },
                    child: Text(
                      Constants.ADD_CART_BUTTON_TEXT.toUpperCase(),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                        color: PRIMARY_TEXT_COLOR,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
