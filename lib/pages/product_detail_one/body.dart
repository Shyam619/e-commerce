import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/pages/cart/cart_page.dart';
import 'package:ecommerce_flutter_app/pages/payment/payment_page.dart';
import 'package:ecommerce_flutter_app/pages/product_detail_one/product_title_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'add_card.dart';
import 'colour_and_Size.dart';
import 'counter_with_fav_btn.dart';
import 'description.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';

class Body extends StatefulWidget {
   final Product product;
  const Body({Key? key, required this.product, }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
 late final Product product;
 int numOfItems = 0;

   @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    product = widget.product;
  }


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return ListView(
    children: <Widget> [
     SizedBox(
       height:size.height,
      child:Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: size.height * 0.3),
            padding: EdgeInsets.only(
              top: size.height * 0.12,
              left: kDefaultPaddin,
              right: kDefaultPaddin,
            ),
            decoration: BoxDecoration(
              color:PRIMARY_TEXT_COLOR,
                borderRadius: BorderRadius.only(
                topLeft: Radius.circular(24),
                topRight: Radius.circular(24),
              )
            ),
          child:ListView(
            shrinkWrap: false,
              children: [
               ColorAndSize(product: product),
                SizedBox(height: kDefaultPaddin / 2),
                Description(product: product),
                SizedBox(height: kDefaultPaddin / 2),
                cartCounter(),
                SizedBox(height: kDefaultPaddin / 2),
                //AddToCart(product: product,)
                addToCart()
              ],
            ),
          ),
          ProductTitleWithImage(product: product)
         ],
       ),
      )
     ],
    );
  }
  Widget cartCounter(){
     return Row(
       children: <Widget>[
         buildOutlineButton(
           icon: Icons.remove,
           press: () {
             if (numOfItems > 0) {
               setState(() {
                 numOfItems--;
               });
             }
           },
         ),
         Padding(
           padding: EdgeInsets.symmetric(horizontal: kDefaultPaddin / 2),
           child: Text(
             // if our item is less  then 10 then  it shows 01 02 like that
               numOfItems.toString().padLeft(2, "0"),
               style: TextStyle(color:PRIMARY_TEXT_COLOR_BLACK)
           ),
         ),
         buildOutlineButton(
             icon: Icons.add,
             press: () {
               setState(() {
                 numOfItems++;
               });
             }),
       ],
     );
  }
 SizedBox buildOutlineButton({required IconData icon, required VoidCallback press}) {
   return SizedBox(
     width: 40,
     height: 32,
     child: OutlineButton(
       padding: EdgeInsets.zero,
       shape: RoundedRectangleBorder(
         borderRadius: BorderRadius.circular(13),
       ),
       onPressed: press,
       child: Icon(icon),
     ),
   );
 }
 Widget addToCart(){
     return Column(
       children: <Widget>[
         //  Container(
         // margin: EdgeInsets.only(right:kDefaultPaddin ),
         //  height: 50.0,
         //  width: 50.0,
         //  decoration: BoxDecoration(
         //    borderRadius:BorderRadius.circular(18),
         //    border:Border.all(
         //     color:Colors.black26
         //    )
         //    ),
         //    // child: IconButton(
         //    //   icon: new Icon(Icons.shopping_cart),
         //    //   onPressed: () {  },
         //    //   color: Colors.black26,
         //    // ),
         //   ),
         // SizedBox(height: 20.0,),
         // Expanded(
         //       child: SizedBox(
         //       height: 50.0,
         //         child: FlatButton(
         //         shape: RoundedRectangleBorder(
         //         borderRadius: BorderRadius.circular(18)),
         //        color:Colors.black,
         //       onPressed: () {},
         //           child: Text(
         //            "Buy Now".toUpperCase(),
         //               style: TextStyle(
         //               fontSize: 17,
         //               fontWeight: FontWeight.bold,
         //               color: Colors.white,
         //               ),
         //           ),
         //       ))
         //   ),
         Container(
           margin: EdgeInsets.symmetric(vertical: 15.0),
           child: Column(
             children: [
               SizedBox(
                 height: PRODUCT_DETAIL_SEIZED_BOX_HEIGHT_BIG,
                 width: 300.0,
                 child: Padding(
                   padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                   child: FlatButton(
                     shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(18)),
                     color: Colors.black,
                     onPressed: () {
                       Navigator.push(
                         context,
                         MaterialPageRoute(
                           builder: (context) =>
                               PaymentPage(totalAmount:product.price,),
                         ),
                       );
                     },
                     child: Text(
                       Constants.BAY_NOW_BUTTON_TEXT.toUpperCase(),
                       style: TextStyle(
                         fontSize: 17,
                         fontWeight: FontWeight.bold,
                         color: PRIMARY_TEXT_COLOR,
                       ),
                     ),
                   ),
                 ),
               ),
               Padding(
                 padding: const EdgeInsets.fromLTRB(0.0, 24.0, 0.0, 0.0),
                 child: SizedBox(
                   height: PRODUCT_DETAIL_SEIZED_BOX_HEIGHT_BIG,
                   child: Text(Constants.BUTTON_SPACE_TEXT.toUpperCase(),
                       style: TextStyle(
                           color: PRIMARY_TEXT_COLOR_BLACK, fontSize: 17)),
                 ),
               ),
               SizedBox(
                 height: PRODUCT_DETAIL_SEIZED_BOX_HEIGHT_BIG,
                 width: 300.0,
                 child: Padding(
                   padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                   child: FlatButton(
                     shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(18)),
                     color: Colors.black,
                     onPressed: () async {
                       final result = await Navigator.push(
                         context,
                         MaterialPageRoute(
                           builder: (context) =>
                               CartPage(product: product, quantity: numOfItems),
                         ),
                       );

                       setState(() {
                         numOfItems = result;
                       });


                     },
                     child: Text(
                       Constants.ADD_CART_BUTTON_TEXT.toUpperCase(),
                       style: TextStyle(
                         fontSize: 17,
                         fontWeight: FontWeight.bold,
                         color: PRIMARY_TEXT_COLOR,
                       ),
                     ),
                   ),
                 ),
               )
             ],
           ),
         )
       ],
     );
 }
}

