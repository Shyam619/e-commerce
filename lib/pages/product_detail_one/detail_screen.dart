
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:ecommerce_flutter_app/pages/product_detail_one/body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  final Product product;
   const DetailScreen({Key? key, required this.product, }) : super(key: key);

   @override
   _DetailScreenState createState() => _DetailScreenState();
  // Size get preferredSize => const Size.fromHeight(100);

 }

 class _DetailScreenState extends State<DetailScreen> {
    late final Product product;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    product = widget.product;
  }


   @override
   Widget build(BuildContext context) {
     return Scaffold(
         backgroundColor: product.color,
         appBar:buildAppBar(context),
         body:Body(product: product)
     );
   }
   PreferredSize buildAppBar(BuildContext context){
    return PreferredSize(
      preferredSize: Size.fromHeight(70),
      child: AppBar(
            elevation: 0.0,
            backgroundColor:Colors.transparent,
            //iconTheme: IconThemeData(color: Colors.green),
            actionsIconTheme: IconThemeData(color: Colors.black54),
            leading: IconButton(icon: Icon(Icons.arrow_back_outlined),
              onPressed:() => Navigator.pop(context),),
            actions: [
              Row(
                children: [
                  // IconButton(icon: Icon(Icons.search),
                  //   onPressed: () {
                  //   },),
                  IconButton(icon: Icon(Icons.shopping_cart),
                    onPressed: () {
                    },),
                ],
              )
            ]
        ),
    );
   }
 }
