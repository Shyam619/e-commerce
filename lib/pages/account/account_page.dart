import 'package:ecommerce_flutter_app/common/asset_path.dart';
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce_flutter_app/pages/account/edit_profile/edit_profile_page.dart';
import 'package:ecommerce_flutter_app/widgets/default_appbar.dart';
import 'package:ecommerce_flutter_app/widgets/default_button.dart';
import 'package:ecommerce_flutter_app/widgets/default_space_horizontal.dart';
import 'package:ecommerce_flutter_app/widgets/default_space_vertical.dart';
import 'package:ecommerce_flutter_app/widgets/min_padding_space_horizontal.dart';
import 'package:flutter/material.dart';

import 'address_book/address_book_page.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
    var size;

    void gotoEditProfilePage() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              EditProfilePage(),
        ),
      );
    }

    void gotoAddressBookPage() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddressBookPage()
        ),
      );
    }
    void gotoFeedBackPage() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DefaultAppBar()
        ),
      );
    }

    void onBackPressed() {
      Navigator.of(context).pop();
    }


  @override
  Widget build(BuildContext context) {

    pageTitle() => Text(
      "My Account",
      style: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    );

    size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children:<Widget> [
          ListView(
            shrinkWrap: false,
             children: <Widget>[
               profileDetails(),
               listOfOptions(),
               signOutButton(),
               SizedBox(height: 70,),
             ],
          )

        ],
      ),
    );
  }

  Widget profileDetails() {
    /// User Image
    userImage() {
      var userImage = CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: PROFILE_IMAGE_RADIUS + 1,
        child: CachedNetworkImage(
          alignment: Alignment.center,
          imageUrl: Constants.USER_IMAGE_URL_ACCOUNT_PAGE,
          imageBuilder: (context, imageProvider) => Container(
            child: CircleAvatar(
              radius: PROFILE_IMAGE_RADIUS,
              backgroundImage: imageProvider,
              backgroundColor: Colors.transparent,
            ),
          ),
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      );

      return Container(
          padding: const EdgeInsets.all(1),
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            border: new Border.all(
              width: 1.0,
              color: Colors.white,
            ),
            boxShadow: [
              new BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 2.0,
                spreadRadius: 1.0,
              ),
            ],
          ),
          child: userImage);
    }

    /// User Name
    var userName = Text(
      "Hello, \nShyam Rudhy",
      style: Theme.of(context)
          .textTheme
          .bodyText2!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    );

    Widget viewMyProfileText() => Text(
          "VIEW MY PROFILE",
          style: Theme.of(context).textTheme.caption!.copyWith(
              fontWeight: FontWeight.bold, color: PRIMARY_TEXT_COLOR_BLACK),
        );

    Widget arrowRight() => Icon(
          Icons.arrow_forward_ios_rounded,
          size: 15,
        );

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2.0,
        shadowColor: Colors.grey.shade500,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(DEFAULT_THEME_CARD_RADIUS),
        ),
        child: Padding(
          padding: const EdgeInsets.all(DEFAULT_PADDING),
          child: Row(

            children: [
              userImage(),
              DefaultSpaceHorizontal(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  userName,
                  DefaultSpaceVertical(),
                  DefaultSpaceVertical(),
                  InkWell(
                    onTap: () {
                      gotoEditProfilePage();
                    },
                    child: Row(
                      children: [
                        viewMyProfileText(),
                        arrowRight(),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget listOfOptions() {

      Widget myOrders() {
        var myOrderText = Text(
          "My Orders",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myOrderText,
              ),divider,
            ],
          ),
        );
      }

      Widget myWishList() {
        var myWishListText = Text(
          "My Wish List",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){

          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myWishListText,
              ),divider,
            ],
          ),
        );
      }

      Widget myAddressBook() {
        var myAddressBookText = Text(
          "Address Book",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){
            gotoAddressBookPage();
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myAddressBookText,
              ),divider,
            ],
          ),
        );
      }

      Widget myLanguage() {
        var myLanguageText = Text(
          "Language",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){

          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myLanguageText,
              ),divider,
            ],
          ),
        );
      }

      Widget myPermission() {
        var myPermissionText = Text(
          "Permissions",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){

          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myPermissionText,
              ),divider,
            ],
          ),
        );
      }

      Widget myFeedback() {
        var myFeedbackText = Text(
          "Feedback",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){

          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myFeedbackText,
              ),divider,
            ],
          ),
        );
      }

      Widget myAboutUs() {
        var myAboutUsText = Text(
          "About us",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){

          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myAboutUsText,
              ),divider,
            ],
          ),
        );
      }


      Widget myPrivacyPolicy() {
        var myPrivacyPolicyText = Text(
          "Privacy Policy",
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,fontWeight: FontWeight.w600),
        );

        var divider = Divider(thickness: 1,);

        return InkWell(
          onTap: (){
            gotoFeedBackPage();
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: myPrivacyPolicyText,
              ),divider,
            ],
          ),
        );
      }

      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: DEFAULT_PADDING,vertical: DEFAULT_PADDING),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            myOrders(),
            myWishList(),
            myAddressBook(),
            myLanguage(),
            myPermission(),
            myFeedback(),
            myAboutUs(),
            myPrivacyPolicy(),
          ],
        ),
      );
  }

  Widget signOutButton() =>Padding(
    padding: const EdgeInsets.all(DEFAULT_PADDING),
    child: DefaultButton(
        size: size,
        onPressed: null,
        buttonText: 'Sign Out',
      ),
  );

}
