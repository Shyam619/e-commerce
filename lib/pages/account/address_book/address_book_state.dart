
class AddressBookPageState<T> {
  AddressBookPageStatus? state;
  T? data;
  String? message;

  AddressBookPageState.loading(this.message) : state = AddressBookPageStatus.LOADING;
  AddressBookPageState.completed(this.data) : state = AddressBookPageStatus.COMPLETED;
  AddressBookPageState.error(this.message) : state = AddressBookPageStatus.ERROR;


  @override
  String toString() {
    return "Status : $state \n Message : $message \n Data : $data";
  }
}

enum AddressBookPageStatus { LOADING, COMPLETED, ERROR }

