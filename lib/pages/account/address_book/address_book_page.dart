import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce_flutter_app/common/asset_path.dart';
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/pages/account/address_book/add_address/add_address_page.dart';
import 'package:ecommerce_flutter_app/widgets/default_button.dart';
import 'package:ecommerce_flutter_app/widgets/default_space_vertical.dart';
import 'package:ecommerce_flutter_app/widgets/min_padding_space_vertical.dart';
import 'package:ecommerce_flutter_app/widgets/mini_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddressBookPage extends StatefulWidget {
  @override
  _AddressBookPageState createState() => _AddressBookPageState();
}

class _AddressBookPageState extends State<AddressBookPage> {
  var size;
  var height;
  var width;

  String? selectedGender;
  bool? _editModeUserName = false;
  bool? _editModeMailId = false;
  bool? _editModeMobileNumber = false;
  bool? _editModeDateOfBirth = false;


  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // GlobalKey is needed to show bottom sheet.
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();



  TextEditingController _userNameTextController = new TextEditingController();
  TextEditingController _mailIdTextController = new TextEditingController();
  TextEditingController _mobileNumberTextController = new TextEditingController();
  TextEditingController _dateOfBirthTextController = new TextEditingController();


  @override
  void dispose() {
    super.dispose();

  }

  @override
  void initState() {
    super.initState();
  }

  void onBackPressed() {
    Navigator.of(context).pop();
  }


  void dismissKeyboard() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  void gotoAddAddressPage(){
     Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddAddressBookPage(),)
    );
  }

  void logoutUser(){
    // profilePageBloc.clearUserSharedPrefDetails();
    //Navigator.pushNamedAndRemoveUntil(context, Constants.LOGIN_PAGE_ROUTE, (Route<dynamic> route) => false);
  }

  void changePassword(){
    // profilePageBloc.updatePassword(_newPasswordTextController.text); //service call
  }


  bool validateInputFields() {
    final form = _formKey.currentState;
    if (form!.validate()) {
      print('***************************Form is valid***************************');
      changePassword();
      return true;
    }
    else {
      print('****************************Form is invalid******************************');
      return false;
    }
  }

/*
  Future<String> getCountryName() async {
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    return first.countryName; // this will return country name
  }*/


/*
  /// Overall Data  service call
  Widget submitNewPassword() => StreamBuilder(
    stream: profilePageBloc.changePasswordStream,
    builder: (context, AsyncSnapshot<ProfilePageState> snapshot) {
      if (snapshot.hasData) {
        switch (snapshot.data.state) {
          case ProfilePageStatus.LOADING:
            return Container(
                color: Colors.white12,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: SECONDARY_COLOR,
                  ),
                ));
            break;

          case ProfilePageStatus.ERROR:
            SchedulerBinding.instance
                .addPostFrameCallback((_) => CommonUtils.showErrorSnackbar(context,snapshot.data.message.toString()));
            break;

          case ProfilePageStatus.COMPLETED:
            SchedulerBinding.instance.addPostFrameCallback((_) {
              CommonUtils.showSuccessSnackbar(
                  context, snapshot.data.message.toString());
            });
            break;
        }
      } else if (snapshot.hasError) {
        return Center(child: Text(snapshot.error.toString()));
      }
      return Container();
    },
  );
*/

  @override
  Widget build(BuildContext context) {

    pageTitle() => Text(
      "Your Addresses",
      style: Theme.of(context)
          .textTheme
          .headline6!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    );

    var personalAddress = Text(
      "Personal address",
      style: Theme.of(context)
          .textTheme
          .headline6!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,),
    );

    size = MediaQuery.of(context).size;
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home: Scaffold(
          key: this._scaffoldKey,
          resizeToAvoidBottomInset: false,
          backgroundColor: BACKGROUND_COLOR,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => onBackPressed(),
            ),
            backgroundColor: BACKGROUND_COLOR,
            title: pageTitle(),
          ),
          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                personalAddress,
                DefaultSpaceVertical(),
                Expanded(
                  flex: 1,
                  child: addressListView(),
                ),
                DefaultSpaceVertical(),
                addAddressButton(),
              ],
            ),
          ),

      ),
    );

  }


  Widget addressListView() {

    Widget addressDetail( ) {

      Widget userName() => Text(
      "Shyam Rudhy V M",
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, fontWeight: FontWeight.w700),
      );

      var flatHouseNoBuildingCompanyApartment = Text(
        "No 2/23",
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,),
      );
   Widget areaStreetSectorVillage() => Text(
        "Gnanambal garden , 1st Street ,Ayanavaram.",
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,),
      );
   Widget townStatePinCode () => Text(
        "chennai , Tamil Nadu 600023".toUpperCase(),
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, ),
      );

   Widget country() => Text(
        "India",
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, ),
      );

   Widget phoneNumber () => Text(
        "phone number: 7200181396",
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK,),
      );

      Widget editButton() => MiniButton(
            size: size,
            onPressed: () {},
            buttonText: 'Edit',
          );

      Widget removeButton() => MiniButton(
            size: size,
            onPressed: () {},
            buttonText: 'Remove',
          );

      return Padding(
        padding: const EdgeInsets.only(bottom: 4),
        child: Card(
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            side:  BorderSide(color: Colors.grey.shade400, width: 1.0),
            borderRadius: BorderRadius.circular(DEFAULT_BORDER_RADIUS),
          ),
          color: BACKGROUND_COLOR,

          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                userName(),
                flatHouseNoBuildingCompanyApartment,
                areaStreetSectorVillage(),
                townStatePinCode(),
                country(),
                phoneNumber(),
                MiniPaddingSpaceVertical(),
                Row(
                  children: [
                    Expanded(child: SizedBox(child: Container(),)),
                    editButton(),
                    Expanded(child: SizedBox(child: Container(),)),
                    removeButton(),
                    Expanded(child: SizedBox(child: Container(),)),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }

    return ListView.builder(
      itemCount:5,
        itemBuilder: (BuildContext context, int index) {
          return addressDetail();
        });
  }


  Widget addAddressButton() =>DefaultButton(
    size: size,
    onPressed:(){
      gotoAddAddressPage();
    },
    buttonText: 'Add address',
  );



}
