import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce_flutter_app/common/asset_path.dart';
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/widgets/default_button.dart';
import 'package:ecommerce_flutter_app/widgets/default_space_vertical.dart';
import 'package:ecommerce_flutter_app/widgets/mini_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AddAddressBookPage extends StatefulWidget {
  @override
  _AddAddressBookPageState createState() => _AddAddressBookPageState();
}

class _AddAddressBookPageState extends State<AddAddressBookPage> {
  var size;
  var height;
  var width;

  List<String> _countryList  = [
    "Afghanistan",
    "Albania",
    "Algeria",
    "American Samoa",
    "Andorra",
    "Angola",
    "Anguilla",
    "Antarctica",
    "Antigua and Barbuda",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahamas (the)",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bermuda",
    "Bhutan",
    "Bolivia (Plurinational State of)",
    "Bonaire, Sint Eustatius and Saba",
    "Bosnia and Herzegovina",
    "Botswana",
    "Bouvet Island",
    "Brazil",
    "British Indian Ocean Territory (the)",
    "Brunei Darussalam",
    "Bulgaria",
    "Burkina Faso",
    "Burundi",
    "Cabo Verde",
    "Cambodia",
    "Cameroon",
    "Canada",
    "Cayman Islands (the)",
    "Central African Republic (the)",
    "Chad",
    "Chile",
    "China",
    "Christmas Island",
    "Cocos (Keeling) Islands (the)",
    "Colombia",
    "Comoros (the)",
    "Congo (the Democratic Republic of the)",
    "Congo (the)",
    "Cook Islands (the)",
    "Costa Rica",
    "Croatia",
    "Cuba",
    "Curaçao",
    "Cyprus",
    "Czechia",
    "Côte d'Ivoire",
    "Denmark",
    "Djibouti",
    "Dominica",
    "Dominican Republic (the)",
    "Ecuador",
    "Egypt",
    "El Salvador",
    "Equatorial Guinea",
    "Eritrea",
    "Estonia",
    "Eswatini",
    "Ethiopia",
    "Falkland Islands (the) [Malvinas]",
    "Faroe Islands (the)",
    "Fiji",
    "Finland",
    "France",
    "French Guiana",
    "French Polynesia",
    "French Southern Territories (the)",
    "Gabon",
    "Gambia (the)",
    "Georgia",
    "Germany",
    "Ghana",
    "Gibraltar",
    "Greece",
    "Greenland",
    "Grenada",
    "Guadeloupe",
    "Guam",
    "Guatemala",
    "Guernsey",
    "Guinea",
    "Guinea-Bissau",
    "Guyana",
    "Haiti",
    "Heard Island and McDonald Islands",
    "Holy See (the)",
    "Honduras",
    "Hong Kong",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran (Islamic Republic of)",
    "Iraq",
    "Ireland",
    "Isle of Man",
    "Israel",
    "Italy",
    "Jamaica",
    "Japan",
    "Jersey",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kiribati",
    "Korea (the Democratic People's Republic of)",
    "Korea (the Republic of)",
    "Kuwait",
    "Kyrgyzstan",
    "Lao People's Democratic Republic (the)",
    "Latvia",
    "Lebanon",
    "Lesotho",
    "Liberia",
    "Libya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macao",
    "Madagascar",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Marshall Islands (the)",
    "Martinique",
    "Mauritania",
    "Mauritius",
    "Mayotte",
    "Mexico",
    "Micronesia (Federated States of)",
    "Moldova (the Republic of)",
    "Monaco",
    "Mongolia",
    "Montenegro",
    "Montserrat",
    "Morocco",
    "Mozambique",
    "Myanmar",
    "Namibia",
    "Nauru",
    "Nepal",
    "Netherlands (the)",
    "New Caledonia",
    "New Zealand",
    "Nicaragua",
    "Niger (the)",
    "Nigeria",
    "Niue",
    "Norfolk Island",
    "Northern Mariana Islands (the)",
    "Norway",
    "Oman",
    "Pakistan",
    "Palau",
    "Palestine, State of",
    "Panama",
    "Papua New Guinea",
    "Paraguay",
    "Peru",
    "Philippines (the)",
    "Pitcairn",
    "Poland",
    "Portugal",
    "Puerto Rico",
    "Qatar",
    "Republic of North Macedonia",
    "Romania",
    "Russian Federation (the)",
    "Rwanda",
    "Réunion",
    "Saint Barthélemy",
    "Saint Helena, Ascension and Tristan da Cunha",
    "Saint Kitts and Nevis",
    "Saint Lucia",
    "Saint Martin (French part)",
    "Saint Pierre and Miquelon",
    "Saint Vincent and the Grenadines",
    "Samoa",
    "San Marino",
    "Sao Tome and Principe",
    "Saudi Arabia",
    "Senegal",
    "Serbia",
    "Seychelles",
    "Sierra Leone",
    "Singapore",
    "Sint Maarten (Dutch part)",
    "Slovakia",
    "Slovenia",
    "Solomon Islands",
    "Somalia",
    "South Africa",
    "South Georgia and the South Sandwich Islands",
    "South Sudan",
    "Spain",
    "Sri Lanka",
    "Sudan (the)",
    "Suriname",
    "Svalbard and Jan Mayen",
    "Sweden",
    "Switzerland",
    "Syrian Arab Republic",
    "Taiwan",
    "Tajikistan",
    "Tanzania, United Republic of",
    "Thailand",
    "Timor-Leste",
    "Togo",
    "Tokelau",
    "Tonga",
    "Trinidad and Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Turks and Caicos Islands (the)",
    "Tuvalu",
    "Uganda",
    "Ukraine",
    "United Arab Emirates (the)",
    "United Kingdom of Great Britain and Northern Ireland (the)",
    "United States Minor Outlying Islands (the)",
    "United States of America (the)",
    "Uruguay",
    "Uzbekistan",
    "Vanuatu",
    "Venezuela (Bolivarian Republic of)",
    "Viet Nam",
    "Virgin Islands (British)",
    "Virgin Islands (U.S.)",
    "Wallis and Futuna",
    "Western Sahara",
    "Yemen",
    "Zambia",
    "Zimbabwe",
    "Åland Islands"
  ];

  String? _selectedCountry; // Option 2




  String? selectedGender;
  bool? _editModeUserName = false;
  bool? _editModeMailId = false;
  bool? _editModeMobileNumber = false;
  bool? _editModeDateOfBirth = false;


  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // GlobalKey is needed to show bottom sheet.
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();



  TextEditingController _userNameTextController = new TextEditingController();
  TextEditingController _mobileNumberTextController = new TextEditingController();
  TextEditingController _pinCodeTextController = new TextEditingController();
  TextEditingController _flatHouseNoBuildingCompanyApartmentTextController = new TextEditingController();
  TextEditingController _areaStreetSectorVillageTextController = new TextEditingController();
  TextEditingController _landmarkTextController = new TextEditingController();
  TextEditingController _townCityTextController = new TextEditingController();
  TextEditingController _stateTextController = new TextEditingController();



  @override
  void dispose() {
    super.dispose();

  }

  @override
  void initState() {
    super.initState();
  }

  void onBackPressed() {
    Navigator.of(context).pop();
  }


  void dismissKeyboard() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  void gotoNextPage(){
    /* Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );*/
  }

  void logoutUser(){
    // profilePageBloc.clearUserSharedPrefDetails();
    //Navigator.pushNamedAndRemoveUntil(context, Constants.LOGIN_PAGE_ROUTE, (Route<dynamic> route) => false);
  }

  void changePassword(){
    // profilePageBloc.updatePassword(_newPasswordTextController.text); //service call
  }


  bool validateInputFields() {
    final form = _formKey.currentState;
    if (form!.validate()) {
      print('***************************Form is valid***************************');
      changePassword();
      return true;
    }
    else {
      print('****************************Form is invalid******************************');
      return false;
    }
  }



/*
  /// Overall Data  service call
  Widget submitNewPassword() => StreamBuilder(
    stream: profilePageBloc.changePasswordStream,
    builder: (context, AsyncSnapshot<ProfilePageState> snapshot) {
      if (snapshot.hasData) {
        switch (snapshot.data.state) {
          case ProfilePageStatus.LOADING:
            return Container(
                color: Colors.white12,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: SECONDARY_COLOR,
                  ),
                ));
            break;

          case ProfilePageStatus.ERROR:
            SchedulerBinding.instance
                .addPostFrameCallback((_) => CommonUtils.showErrorSnackbar(context,snapshot.data.message.toString()));
            break;

          case ProfilePageStatus.COMPLETED:
            SchedulerBinding.instance.addPostFrameCallback((_) {
              CommonUtils.showSuccessSnackbar(
                  context, snapshot.data.message.toString());
            });
            break;
        }
      } else if (snapshot.hasError) {
        return Center(child: Text(snapshot.error.toString()));
      }
      return Container();
    },
  );
*/

Future getCountriesDetail() async {
  String jsonCountry = await rootBundle.loadString(
      "lib/models/countries/json_file/countries_details.json");
  Map _mapCountry = jsonDecode(jsonCountry);
  List list = _mapCountry["countries"];



}



  @override
  Widget build(BuildContext context) {

    pageTitle() => Text(
      "Add a new address",
      style: Theme.of(context)
          .textTheme
          .headline6!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    );

    size = MediaQuery.of(context).size;
    return MaterialApp(

      debugShowCheckedModeBanner: false,
      home: Scaffold(

          key: this._scaffoldKey,
          resizeToAvoidBottomInset: true,
          backgroundColor: BACKGROUND_COLOR,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => onBackPressed(),
            ),
            backgroundColor: BACKGROUND_COLOR,
            title: pageTitle(),
          ),
          body: Stack(
            children:<Widget> [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: ListView(
                children: [
                  DefaultSpaceVertical(),
                   countriesListDropDown(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                   userNameTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                   mobileNumberTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                    pinCodeTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                   flatHouseNoBuildingCompanyApartmentTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                   areaStreetSectorVillageTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                   landmarkTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                   townCityTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                   stateTextField(),
                  SizedBox(height: DEFAULT_PADDING_MAX,),
                  saveAddressButton(),

                ],
                  ),
              ),

            ],
          ),

      ),
    );

  }



  Widget countriesListDropDown() => InputDecorator(
    decoration: const InputDecoration(border: OutlineInputBorder()),
    child: DropdownButtonHideUnderline(

      child:   DropdownButton(
        isExpanded: true,
        elevation: 8,
        hint: Text('India'), // Not necessary for Option 1
        value: _selectedCountry,
        onChanged: (newValue) {
          setState(() {
            _selectedCountry = newValue as String?;
          });
        },
        items: _countryList.map((language) {
          return DropdownMenuItem(
            child:  Text(language,style: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(fontWeight: FontWeight.w500, color: PRIMARY_COLOR),
              textAlign: TextAlign.center,),
            value: language,
          );
        }).toList(),
      )
    ),
  );

  Widget userNameTextField() => TextFormField(
        enabled: true,
        autofocus: false,
        decoration: new InputDecoration(
          isDense: true,
          labelText: Constants.EDIT_PROFILE_PAGE_USER_NAME_HINT,
          labelStyle: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(
              color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
          counterText: "",
          border: new UnderlineInputBorder(
            borderSide: new BorderSide(color: PRIMARY_COLOR),
          ),

          enabledBorder: new UnderlineInputBorder(
            borderSide: new BorderSide(color: PRIMARY_COLOR),
          ),

          focusedBorder: new UnderlineInputBorder(
            borderSide: new BorderSide(color: SECONDARY_COLOR),
          ),

          errorBorder: new UnderlineInputBorder(
            borderSide: new BorderSide(color: ERROR_COLOR),
          ),

          focusedErrorBorder: new UnderlineInputBorder(
            borderSide: new BorderSide(color: ERROR_COLOR),
          ),
          //fillColor: Colors.green
        ),
        validator: (val) {
          if (val!.length < 3) {
            return Constants.EDIT_PROFILE_PAGE_USER_NAME_ERROR;
          } else {
            return null;
          }
        },
        keyboardType: TextInputType.text,
        maxLines: 1,
        maxLength: 30,
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
        controller: _userNameTextController,
      );

  Widget mobileNumberTextField() => TextFormField(
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.EDIT_PROFILE_PAGE_MOBILE_NUMBER_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "",
      border: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: SECONDARY_COLOR),
      ),

      errorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.EDIT_PROFILE_PAGE_MOBILE_NUMBER_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.phone,
    autocorrect: false,
    maxLines: 1,
    maxLength: 10,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _mobileNumberTextController,
  );

  Widget pinCodeTextField() => TextFormField(
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.ADD_ADDRESS_PAGE_PIN_CODE_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "6 digits[0-9] PIN code",
      border: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: SECONDARY_COLOR),
      ),

      errorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.ADD_ADDRESS_PAGE_PIN_CODE_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.phone,
    autocorrect: false,
    maxLines: 1,
    maxLength: 6,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _pinCodeTextController,
  );

  Widget flatHouseNoBuildingCompanyApartmentTextField() => TextFormField(
    enabled: true,
    autofocus: false,
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.ADD_ADDRESS_PAGE_BUILDING_NUMBER_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(
          color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "",
      border: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: SECONDARY_COLOR),
      ),

      errorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.ADD_ADDRESS_PAGE_BUILDING_NUMBER_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.text,
    maxLines: 1,
    maxLength: 30,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _flatHouseNoBuildingCompanyApartmentTextController,
  );

  Widget areaStreetSectorVillageTextField() => TextFormField(
    enabled: true,
    autofocus: false,
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.ADD_ADDRESS_PAGE_AREA_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(
          color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "",
      border: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: SECONDARY_COLOR),
      ),

      errorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.ADD_ADDRESS_PAGE_AREA_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.text,
    maxLines: 1,
    maxLength: 255,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _areaStreetSectorVillageTextController,
  );

  Widget landmarkTextField() => TextFormField(
    enabled: true,
    autofocus: false,
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.ADD_ADDRESS_PAGE_LANDMARK_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(
          color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "eg: near axis bank",
      border: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: SECONDARY_COLOR),
      ),

      errorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.ADD_ADDRESS_PAGE_LANDMARK_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.text,
    maxLines: 1,
    maxLength: 255,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _landmarkTextController,
  );

  Widget townCityTextField() => TextFormField(
    enabled: true,
    autofocus: false,
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.ADD_ADDRESS_PAGE_CITY_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(
          color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "",
      border: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: SECONDARY_COLOR),
      ),

      errorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.ADD_ADDRESS_PAGE_CITY_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.text,
    maxLines: 1,
    maxLength: 255,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _townCityTextController,
  );

  Widget stateTextField() => TextFormField(
    enabled: true,
    autofocus: false,
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.ADD_ADDRESS_PAGE_STATE_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(
          color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "",
      border: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: SECONDARY_COLOR),
      ),

      errorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new UnderlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.ADD_ADDRESS_PAGE_STATE_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.text,
    maxLines: 1,
    maxLength: 255,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _stateTextController,
  );

  Widget saveAddressButton() =>Padding(
    padding: const EdgeInsets.all(DEFAULT_PADDING),
    child: DefaultButton(
      size: size,
      onPressed:(){},
      buttonText: 'Save address',
    ),
  );



}
