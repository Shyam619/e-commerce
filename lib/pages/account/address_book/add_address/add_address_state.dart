
class AddAddressPageState<T> {
  AddAddressPageStatus? state;
  T? data;
  String? message;

  AddAddressPageState.loading(this.message) : state = AddAddressPageStatus.LOADING;
  AddAddressPageState.completed(this.data) : state = AddAddressPageStatus.COMPLETED;
  AddAddressPageState.error(this.message) : state = AddAddressPageStatus.ERROR;


  @override
  String toString() {
    return "Status : $state \n Message : $message \n Data : $data";
  }
}

enum AddAddressPageStatus { LOADING, COMPLETED, ERROR }

