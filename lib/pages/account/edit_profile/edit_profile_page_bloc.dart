import 'dart:async';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/shared_pref_util.dart';
import 'package:ecommerce_flutter_app/models/user_profile_detail.dart';
import 'package:ecommerce_flutter_app/pages/account/edit_profile/edit_profile_page_state.dart';
import 'package:ecommerce_flutter_app/services/api_model/profile_response.dart';
import 'package:ecommerce_flutter_app/services/repository.dart';
import 'package:rxdart/rxdart.dart';

class ProfilePageBloc {
  final _repository = Repository();


  PublishSubject<EditProfilePageState<ProfileResponse>> _profilePageController = PublishSubject<EditProfilePageState<ProfileResponse>>();
  Stream<EditProfilePageState<ProfileResponse>> get profilePageStream => _profilePageController.stream;
  StreamSink<EditProfilePageState<ProfileResponse>> get profilePageSink => _profilePageController.sink;

  UserProfileDetail? _userProfileDetail ;
/*
  getProfileTechnicianDetail() => _myProfileTechnicianDetail;

  fetchAllProfileTechnicianDetail() async{
    try {
      profilePageSink.add(EditProfilePageState.loading(Constants.STATE_LOADING));
      getTechnicianId().then((id) async {
        profilePageSink.add(EditProfilePageState.loading(Constants.STATE_LOADING));
        ProfileResponse response =await _repository.getAllProfileTechnicianDetail(id);

        if(response.status) {
          profilePageSink.add(EditProfilePageState.completed(response));
          _userProfileDetail = response.data;
        } else {
          profilePageSink.add(EditProfilePageState.error(response.message));
        }
      });

    }
    catch (e) {
      profilePageSink.add(EditProfilePageState.error(e.toString()));
    }

  }

  Future<int> getTechnicianId() async {
    return  SharedPrefUtils.getIntData(Constants.TECHNICIAN_ID_KEY)?? null;
  }

  void clearTechnicianDetails() async{
    SharedPrefUtils.clearSharedPreferenceData();
    print("Removed Shared Preference :"+ Constants.TECHNICIAN_ID.toString() +" "+Constants.TECHNICIAN_NAME);

  }*/

  init() {
    _profilePageController = PublishSubject<EditProfilePageState<ProfileResponse>>();
  }

  dispose() {
    _profilePageController.close();
  }

}

final profilePageBloc = ProfilePageBloc();