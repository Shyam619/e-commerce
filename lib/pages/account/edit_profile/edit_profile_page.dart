import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ecommerce_flutter_app/common/asset_path.dart';
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/widgets/default_button.dart';
import 'package:ecommerce_flutter_app/widgets/default_space_vertical.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  var size;
  var height;
  var width;
  bool isMale =  true;
  bool isFemale =  false;
  String? selectedGender;
  bool? _editModeUserName = false;
  bool? _editModeMailId = false;
  bool? _editModeMobileNumber = false;
  bool? _editModeDateOfBirth = false;

  List<String> _languageSpoken = ['English', 'Tamil', 'Hindi']; // Option 2
  String? _selectedLanguageSpoken; // Option 2


  String userName = "User Name";
  String userEmailId ="example@gmail.com";
  String mobileNumber ="+917200181396";

  bool _showPasswordNew = false;
  bool _showPasswordConfirm = false;

  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // GlobalKey is needed to show bottom sheet.
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();



  TextEditingController _userNameTextController = new TextEditingController();
  TextEditingController _mailIdTextController = new TextEditingController();
  TextEditingController _mobileNumberTextController = new TextEditingController();
  TextEditingController _dateOfBirthTextController = new TextEditingController();


  @override
  void dispose() {
    super.dispose();
    _dateOfBirthTextController.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  void onBackPressed() {
    Navigator.of(context).pop();
  }


  void dismissKeyboard() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  void gotoNextPage(){
   /* Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );*/
  }

  void logoutUser(){
   // profilePageBloc.clearUserSharedPrefDetails();
    //Navigator.pushNamedAndRemoveUntil(context, Constants.LOGIN_PAGE_ROUTE, (Route<dynamic> route) => false);
  }

  void changePassword(){
   // profilePageBloc.updatePassword(_newPasswordTextController.text); //service call
  }


  bool validateInputFields() {
    final form = _formKey.currentState;
    if (form!.validate()) {
      print('***************************Form is valid***************************');
      changePassword();
      return true;
    }
    else {
      print('****************************Form is invalid******************************');
      return false;
    }
  }


  DateTime currentDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(1920),
        lastDate: DateTime.now());
    if(pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = pickedDate;
      });
  }

/*
  /// Overall Data  service call
  Widget submitNewPassword() => StreamBuilder(
    stream: profilePageBloc.changePasswordStream,
    builder: (context, AsyncSnapshot<ProfilePageState> snapshot) {
      if (snapshot.hasData) {
        switch (snapshot.data.state) {
          case ProfilePageStatus.LOADING:
            return Container(
                color: Colors.white12,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: SECONDARY_COLOR,
                  ),
                ));
            break;

          case ProfilePageStatus.ERROR:
            SchedulerBinding.instance
                .addPostFrameCallback((_) => CommonUtils.showErrorSnackbar(context,snapshot.data.message.toString()));
            break;

          case ProfilePageStatus.COMPLETED:
            SchedulerBinding.instance.addPostFrameCallback((_) {
              CommonUtils.showSuccessSnackbar(
                  context, snapshot.data.message.toString());
            });
            break;
        }
      } else if (snapshot.hasError) {
        return Center(child: Text(snapshot.error.toString()));
      }
      return Container();
    },
  );
*/

  @override
  Widget build(BuildContext context) {

    pageTitle() => Text(
      "Edit Profile",
      style: Theme.of(context)
          .textTheme
          .headline6!
          .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    );

    size = MediaQuery.of(context).size;
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home: Scaffold(
          key: this._scaffoldKey,
          resizeToAvoidBottomInset: false,
          backgroundColor: BACKGROUND_COLOR,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => onBackPressed(),
            ),
            backgroundColor: BACKGROUND_COLOR,
            title: pageTitle(),
            actions: [
              /*IconButton(
                padding: EdgeInsets.all(DEFAULT_MIN_PADDING_TWELVE),
                icon: Image.asset(AssetsPath.LOGOUT_ICON),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) => confirmationAlertBox(context));
                },
              ),*/
            ],
          ),
          body: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(DEFAULT_PADDING),
                child: profileContainer(),

              ),
              saveButton(),
              //submitNewPassword(),
            ],
          )),
    );
  }





  Widget profileContainer() {

    /// User Image
    Widget userImage()  {
      var userImage = CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: PROFILE_IMAGE_RADIUS + 1,
        child: CachedNetworkImage(
          alignment: Alignment.center,
          imageUrl: Constants.USER_IMAGE_URL_ACCOUNT_PAGE,
          imageBuilder: (context, imageProvider) =>
              Container(
                child: CircleAvatar(
                  radius: PROFILE_IMAGE_RADIUS,
                  backgroundImage: imageProvider,
                  backgroundColor: Colors.transparent,
                ),
              ),
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      );

      return Stack(
        children: [
          Container(
              padding: const EdgeInsets.all(1),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                border: new Border.all(
                  width: 1.0,
                  color: Colors.white,
                ),
                boxShadow: [
                  new BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    blurRadius: 2.0,
                    spreadRadius: 1.0,
                  ),
                ],
              ),
              child: userImage),
          Positioned(
            bottom: EDIT_ICON_POSITION_BOTTOM,
            right: EDIT_ICON_POSITION_RIGHT,
            child: Container(
              width: DEFAULT_WIDTH_HEIGHT_THIRTY_TWO,
              height: DEFAULT_WIDTH_HEIGHT_THIRTY_TWO,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: SECONDARY_COLOR,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.transparent,
                        offset: Offset(0.0, 1.0),
                        blurRadius: 0.0)
                  ]),
              child: Padding(
                padding: const EdgeInsets.all(DEFAULT_VERY_MIN_PADDING),
                child: GestureDetector(
                    onTap: (){
                    },
                    child: Image(image: AssetImage(AssetsPath.EDIT_ICON))),
              ),
            ),
          ),
        ],
      );
    }
    Widget userNameText() => Text(
      userName,
      style: Theme.of(context)
          .textTheme
          .headline5!
          .copyWith(
        color: PRIMARY_TEXT_COLOR_BLACK,
        fontWeight: FontWeight.w400,
      ),
    );
    Widget userEmailText() => Text(
      userEmailId,
      style: Theme.of(context)
          .textTheme
          .caption!
          .copyWith(
        color: PRIMARY_TEXT_COLOR_BLACK,
        fontWeight: FontWeight.w400,
      ),
    );
    Widget userDesignation() => Text(
      mobileNumber,
      style: Theme.of(context)
          .textTheme
          .bodyText1!
          .copyWith(
        color: PRIMARY_TEXT_COLOR_BLACK,
        fontWeight: FontWeight.w800,
      ),
    );

    Widget userNameTextField() => Stack(
          children: [
            _editModeUserName == false
                ? TextFormField(
                    enabled: false,
                    decoration: new InputDecoration(
                      isDense: true,
                      labelText: Constants.EDIT_PROFILE_PAGE_USER_NAME_HINT,
                      labelStyle: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(
                              color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
                      counterText: "",
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      focusedBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: SECONDARY_COLOR),
                      ),

                      errorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),

                      focusedErrorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),
                      //fillColor: Colors.green
                    ),
                    validator: (val) {
                      if (val!.length < 3) {
                        return Constants.EDIT_PROFILE_PAGE_USER_NAME_ERROR;
                      } else {
                        return null;
                      }
                    },
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    maxLength: 30,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: SECONDARY_COLOR,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                    controller: _userNameTextController,
                  )
                : TextFormField(
                    enabled: true,
                    autofocus: true,
                    decoration: new InputDecoration(
                      isDense: true,
                      labelText: Constants.EDIT_PROFILE_PAGE_USER_NAME_HINT,
                      labelStyle: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(
                              color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
                      counterText: "",
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      focusedBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: SECONDARY_COLOR),
                      ),

                      errorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),

                      focusedErrorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),
                      //fillColor: Colors.green
                    ),
                    validator: (val) {
                      if (val!.length < 3) {
                        return Constants.EDIT_PROFILE_PAGE_USER_NAME_ERROR;
                      } else {
                        return null;
                      }
                    },
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    maxLength: 30,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
                    controller: _userNameTextController,
                  ),
            Align(
              alignment: Alignment.centerRight,
              child: _editModeUserName == false
                  ? IconButton(
                      icon: Icon(
                        Icons.edit_off_rounded,
                        size: 20.0,
                      ),
                      onPressed: () {
                        setState(() {
                          _editModeUserName = !_editModeUserName!;
                        });
                      })
                  : IconButton(
                      icon: Icon(
                        Icons.edit,
                        size: 20.0,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        setState(() {
                          _editModeUserName = !_editModeUserName!;
                        });
                      }),
            )
          ],
        );

    Widget mailIDTextField() => Stack(
          children: [
            _editModeMailId == false
                ? TextFormField(
                    enabled: false,
                    decoration: new InputDecoration(
                      isDense: true,
                      labelText: Constants.EDIT_PROFILE_PAGE_EMAIL_ID_HINT,
                      labelStyle: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(
                              color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
                      counterText: "",
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      focusedBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: SECONDARY_COLOR),
                      ),

                      errorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),

                      focusedErrorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),
                      //fillColor: Colors.green
                    ),
                    validator: (val) {
                      if (val!.length < 3) {
                        return Constants.EDIT_PROFILE_PAGE_EMAIL_ID_ERROR;
                      } else {
                        return null;
                      }
                    },
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    maxLength: 30,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: SECONDARY_COLOR,
                        fontWeight: FontWeight.w700,
                        fontSize: 16),
                    controller: _mailIdTextController,
                  )
                : TextFormField(
                    enabled: true,
                    autofocus: true,
                    decoration: new InputDecoration(
                      isDense: true,
                      labelText: Constants.EDIT_PROFILE_PAGE_EMAIL_ID_ERROR,
                      labelStyle: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(
                              color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
                      counterText: "",
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      enabledBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: PRIMARY_COLOR),
                      ),

                      focusedBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: SECONDARY_COLOR),
                      ),

                      errorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),

                      focusedErrorBorder: new UnderlineInputBorder(
                        borderSide: new BorderSide(color: ERROR_COLOR),
                      ),
                      //fillColor: Colors.green
                    ),
                    validator: (val) {
                      if (val!.length < 3) {
                        return Constants.EDIT_PROFILE_PAGE_EMAIL_ID_ERROR;
                      } else {
                        return null;
                      }
                    },
                    keyboardType: TextInputType.text,
                    maxLines: 1,
                    maxLength: 30,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
                    controller: _mailIdTextController,
                  ),
            Align(
              alignment: Alignment.centerRight,
              child: _editModeMailId == false
                  ? IconButton(
                      icon: Icon(
                        Icons.edit_off_rounded,
                        size: 20.0,
                      ),
                      onPressed: () {
                        setState(() {
                          _editModeMailId = !_editModeMailId!;
                        });
                      })
                  : IconButton(
                      icon: Icon(Icons.edit, size: 20.0, color: Colors.grey),
                      onPressed: () {
                        setState(() {
                          _editModeMailId = !_editModeMailId!;
                        });
                      }),
            )
          ],
        );

    Widget mobileNumberTextField() => TextFormField(
          decoration: new InputDecoration(
            isDense: true,
            labelText: Constants.EDIT_PROFILE_PAGE_MOBILE_NUMBER_HINT,
            labelStyle: Theme.of(context)
                .textTheme
                .subtitle1!
                .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
            counterText: "",
            border: new UnderlineInputBorder(
              borderSide: new BorderSide(color: PRIMARY_COLOR),
            ),

            enabledBorder: new UnderlineInputBorder(
              borderSide: new BorderSide(color: PRIMARY_COLOR),
            ),

            focusedBorder: new UnderlineInputBorder(
              borderSide: new BorderSide(color: SECONDARY_COLOR),
            ),

            errorBorder: new UnderlineInputBorder(
              borderSide: new BorderSide(color: ERROR_COLOR),
            ),

            focusedErrorBorder: new UnderlineInputBorder(
              borderSide: new BorderSide(color: ERROR_COLOR),
            ),
            //fillColor: Colors.green
          ),
          validator: (val) {
            if (val!.length < 3) {
              return Constants.EDIT_PROFILE_PAGE_MOBILE_NUMBER_ERROR;
            } else {
              return null;
            }
          },
          keyboardType: TextInputType.phone,
          autocorrect: false,
          maxLines: 1,
          maxLength: 10,
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
          controller: _mobileNumberTextController,
        );

    Widget dateOfBirthTextField() => Stack(
      children: [

        TextFormField(
              decoration: new InputDecoration(
                isDense: true,
                labelText: Constants.EDIT_PROFILE_PAGE_DOB_HINT,
                labelStyle: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
                counterText: "",
                border: new UnderlineInputBorder(
                  borderSide: new BorderSide(color: PRIMARY_COLOR),
                ),

                enabledBorder: new UnderlineInputBorder(
                  borderSide: new BorderSide(color: PRIMARY_COLOR),
                ),

                focusedBorder: new UnderlineInputBorder(
                  borderSide: new BorderSide(color: SECONDARY_COLOR),
                ),

                errorBorder: new UnderlineInputBorder(
                  borderSide: new BorderSide(color: ERROR_COLOR),
                ),

                focusedErrorBorder: new UnderlineInputBorder(
                  borderSide: new BorderSide(color: ERROR_COLOR),
                ),
                //fillColor: Colors.green
              ),
              validator: (val) {
                RegExp regExp = new RegExp(
                  r"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$",
                  caseSensitive: true,
                  multiLine: false,
                );

                if (regExp.hasMatch(val!)) {
                  return Constants.EDIT_PROFILE_PAGE_DOB_ERROR;
                } else {
                  return null;
                }
              },
              keyboardType: TextInputType.text,
              enabled: false,
              autocorrect: false,
              maxLines: 1,
              maxLength: 10,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: SECONDARY_COLOR),
              controller: _dateOfBirthTextController..text = currentDate.year.toString() + "-" + currentDate.month.toString() + "-" + currentDate.day.toString()
           ,
          onChanged: (text) => {},
        ),
        Align(
          alignment: Alignment.centerRight,
          child: IconButton(
              icon: Icon(Icons.calendar_today_sharp, size: 20.0,),
              onPressed: () {
               _selectDate(context);
                FocusScope.of(context).requestFocus(new FocusNode());
                setState(() {
                  _dateOfBirthTextController.clear();
                  _dateOfBirthTextController =_dateOfBirthTextController;

                });
              }),
        )
      ],
    );

    Widget femaleContainer() =>Material(
      borderRadius:  BorderRadius.all(Radius.circular(DEFAULT_BORDER_RADIUS),),
      color: WHITE_COLOR,
      child: InkWell(
        child: Container(
            padding: EdgeInsets.all(DEFAULT_PADDING),
            decoration: isFemale
                ? BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(DEFAULT_BORDER_RADIUS),
                ),
                border: Border.all(color: SECONDARY_COLOR))
                : null,
            child: Row(
              children: [
                isFemale ?
                Text(
                  Constants.EDIT_PROFILE_FEMALE,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .copyWith(fontWeight: FontWeight.w500, color: SECONDARY_COLOR),
                  textAlign: TextAlign.center,
                ):
                Text(
                  Constants.EDIT_PROFILE_FEMALE,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .copyWith(fontWeight: FontWeight.w500, color: PRIMARY_COLOR),
                  textAlign: TextAlign.center,
                ),
                SizedBox(width: DEFAULT_PADDING),
                isFemale ? Icon(Icons.circle, color: SECONDARY_COLOR) :  Icon(Icons.circle, color: PRIMARY_COLOR,)
              ],
            )
        ),
        onTap: () {
          setState(() {
            isFemale = true;
            isMale = false;
            selectedGender ="FEMALE";
          });
        },
      ),
    );

    Widget maleContainer() => Material(
      borderRadius:  BorderRadius.all(Radius.circular(DEFAULT_BORDER_RADIUS),),
      color: WHITE_COLOR,
      child: InkWell(
        child: Container(
            padding: EdgeInsets.all(DEFAULT_PADDING),
            decoration: isMale
                ? BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(DEFAULT_BORDER_RADIUS),
                ),
                border: Border.all(color: SECONDARY_COLOR))
                : null,
            child: Row(
              children: [
                isMale ?
                Text(
                  Constants.EDIT_PROFILE_MALE,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .copyWith(fontWeight: FontWeight.w500, color: SECONDARY_COLOR),
                  textAlign: TextAlign.center,
                ):
                Text(
                  Constants.EDIT_PROFILE_MALE,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2!
                      .copyWith(fontWeight: FontWeight.w500, color: PRIMARY_COLOR),
                  textAlign: TextAlign.center,
                ),
                SizedBox(width: DEFAULT_PADDING),
                isMale ? Icon(Icons.circle, color: SECONDARY_COLOR) :  Icon(Icons.circle, color: PRIMARY_COLOR,)
              ],
            )
        ),
        onTap: () {
          setState(() {
            isFemale = false;
            isMale = true;
            selectedGender ="MALE";
          });
        },
      ),
    );

    Widget languageSpoken() =>  DropdownButton(
      isExpanded: true,

      hint: Text('Language Spoken'), // Not necessary for Option 1
      value: _selectedLanguageSpoken,
      onChanged: (newValue) {
        setState(() {
          _selectedLanguageSpoken = newValue as String?;
        });
      },
      items: _languageSpoken.map((language) {
        return DropdownMenuItem(
          child: new Text(language,style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(fontWeight: FontWeight.w500, color: PRIMARY_COLOR),
            textAlign: TextAlign.center,),
          value: language,
        );
      }).toList(),
    );

    var aboutMeText = Align(
    alignment: Alignment.centerLeft,
    child: Text(
      Constants.EDIT_PROFILE_ABOUT_ME_TEXT,
      style: Theme.of(context)
          .textTheme
          .bodyText2!
          .copyWith(fontWeight: FontWeight.w500, color: PRIMARY_COLOR),
      textAlign: TextAlign.center,
    ),
);

    Widget aboutMe() => Card(
          color: WHITE_COLOR,
          child: Padding(
            padding: EdgeInsets.all(1.0),
            child: TextField(
                cursorColor: PRIMARY_COLOR,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: SECONDARY_COLOR, width: 1.0,
                    ),
                  ),
                ),
                maxLines: 3,
                keyboardType: TextInputType.multiline,
                style: TextStyle(
                  color:PRIMARY_COLOR,
                )
            ),

          ),
        );

    return Column(
      children: <Widget>[
        userImage(),
        SizedBox(height: DEFAULT_PADDING,),
        userNameTextField(),
        SizedBox(height: DEFAULT_PADDING_MAX,),
        mailIDTextField(),
        SizedBox(height: DEFAULT_PADDING_MAX,),
        mobileNumberTextField(),
        SizedBox(height: DEFAULT_PADDING_MAX,),
        Row(
          children: [
            Expanded(child: SizedBox(child: Container(),)),
            maleContainer(),
            Expanded(child: SizedBox(child: Container(),)),
            femaleContainer(),
            Expanded(child: SizedBox(child: Container(),)),
          ],
        ),
        SizedBox(height: DEFAULT_PADDING_MAX,),
        dateOfBirthTextField(),
        SizedBox(height: DEFAULT_PADDING_MAX,),
        languageSpoken(),
        SizedBox(height: DEFAULT_PADDING_MAX,),
        aboutMeText,
        SizedBox(height: DEFAULT_PADDING_MAX,),
        aboutMe(),
        ],
    );
  }

  Widget saveButton() =>Padding(
    padding: const EdgeInsets.all(DEFAULT_PADDING),
    child: DefaultButton(
      size: size,
      onPressed:(){},
      buttonText: 'Save',
    ),
  );


}
