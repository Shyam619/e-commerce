
class EditProfilePageState<T> {
  EditProfilePageStatus? state;
  T? data;
  String? message;

  EditProfilePageState.loading(this.message) : state = EditProfilePageStatus.LOADING;
  EditProfilePageState.completed(this.data) : state = EditProfilePageStatus.COMPLETED;
  EditProfilePageState.error(this.message) : state = EditProfilePageStatus.ERROR;


  @override
  String toString() {
    return "Status : $state \n Message : $message \n Data : $data";
  }
}

enum EditProfilePageStatus { LOADING, COMPLETED, ERROR }

