import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:ecommerce_flutter_app/pages/account/account_page.dart';
import 'package:ecommerce_flutter_app/pages/cart/cart_page.dart';
import 'package:ecommerce_flutter_app/pages/dashboard/dashboard_page.dart';
import 'package:ecommerce_flutter_app/pages/product_list/product_screen.dart';
import 'package:ecommerce_flutter_app/widgets/search_delegate.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  late Product product;
  List<String> searchDataTitle =[];

  /*void seperateSearchData() {

    for(int i=0;i<=products.length;i++) {
      searchDataTitle.add(products[i].title);

    }
print(searchDataTitle);
  }*/

  List screens = [
    // Dashboard(),
    Dashboard(),
    HomeScreen(),
    CartPage(product: products[0], quantity: 1,),
    AccountPage(),
  ];

  @override
  void initState() {
    super.initState();
    /*seperateSearchData();*/
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onNavigationItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  shopPageTitle() => Text(
        Constants.SHOP_PAGE_HEADING,
        style: Theme.of(context)
            .textTheme
            .subtitle1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
      );

  categoryPageTitle() => Text(
        Constants.CATEGORY_PAGE_HEADING,
        style: Theme.of(context)
            .textTheme
            .subtitle1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
      );

  cartPageTitle() => Text(
        Constants.CART_PAGE_HEADING,
        style: Theme.of(context)
            .textTheme
            .subtitle1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
      );

  pageTitle() => Text(
        Constants.ACCOUNT_PAGE_HEADING,
        style: Theme.of(context)
            .textTheme
            .subtitle1!
            .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
      );

  AppBar categoryAppBar() {
    return AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        actionsIconTheme: IconThemeData(color: PRIMARY_COLOR),
        /*leading: Icon(Icons.arrow_back_outlined,color: Colors.black54,),*/
        title: categoryPageTitle(),
        actions: [
          Row(
            children: [
              IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.notification_important_outlined),
                onPressed: () {},
              ),
            ],
          )
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _selectedIndex == 0
          ? AppBar(
              title: shopPageTitle(),
              actions: [
                IconButton(
                        onPressed: () {

                          showSearch(context: context, delegate: DataSearch());
                        },
                        icon: Icon(Icons.search)),
                IconButton(
                  icon: Icon(Icons.notification_important_outlined),
                  onPressed: () {},
                ),
              ],
              elevation: 0.0,
              /*backgroundColor: Colors.blueGrey,*/
            )
          : _selectedIndex == 1
              ? categoryAppBar()
              : _selectedIndex == 2
                  ? AppBar(
                      title: cartPageTitle(),
                      elevation: 0.0,
                      /*backgroundColor: Colors.blueGrey,*/
                      actions: [
                        IconButton(
                          icon: Icon(Icons.notification_important_outlined),
                          onPressed: () {},
                        ),
                      ],
                    )
                  : AppBar(
                      title: pageTitle(),
                    ),
      body: Stack(
        children: [
          screens[_selectedIndex],
          Align(
            alignment: Alignment.bottomCenter,
            child: bottomNavigation(context),
          )
        ],
      ),
    );
  }

  Widget bottomNavigation(context) => BottomNavigationBar(
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.blueGrey,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: 'Shop',
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.category_outlined), label: 'Category'),
          BottomNavigationBarItem(
              icon: Icon(Icons.add_shopping_cart), label: 'Cart'),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined), label: 'Account'),
        ],
        currentIndex: _selectedIndex,
        onTap: _onNavigationItemTapped,
        elevation: 8.0,
      );
}
