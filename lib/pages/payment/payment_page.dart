import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/common_utils.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/pages/home/home_page.dart';
import 'package:ecommerce_flutter_app/widgets/default_space_vertical.dart';
import 'package:flutter/material.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class PaymentPage extends StatefulWidget {
  int totalAmount;

  PaymentPage({Key? key, required this.totalAmount}) : super(key: key);

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  late Razorpay razorpay;

  int totalAmount = 0;
  var size;

  @override
  void initState() {
    super.initState();
    totalAmount = widget.totalAmount;

    razorpay = new Razorpay();
    razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
    razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, handlerErrorFailure);
    razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
    razorpay.clear();
  }

  void openCheckout() {
    var options = {
      "key": "rzp_test_9rt1hX38C2uLnJ",
      "amount": totalAmount * 100,
      "name": "Buy It!",
      "description": "Payment for the product",
      "prefill": {"contact": "8870810709", "email": "naveenb@khojguru.com"},
      "external": {
        "wallets": ["paytm"]
      }
    };

    try {
      razorpay.open(options);
    } catch (e) {
      print(e.toString());
    }
  }

  void handlerPaymentSuccess() {
    print(
        "*****************************Payment Successful*************************");
  }

  void handlerErrorFailure() {
    print("Payment error");
  }

  void handlerExternalWallet() {
    print("External Wallet");
  }

  Widget button() => Container(
        width: size.width * 1,
        height: size.height * 0.07,
        decoration: ShapeDecoration(
          color: SECONDARY_WHITE_COLOR,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(5.0),
              bottomRight: Radius.circular(5.0),
              topLeft: Radius.circular(5.0),
              topRight: Radius.circular(5.0),
            ),
          ),
        ),
        child: new MaterialButton(
            onPressed: () {
              openCheckout();
              /*Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                );*/
            },
            child: Text(
              "Pay by RazorPay",
              style: Theme.of(context).textTheme.headline5!.copyWith(
                    color: Colors.blue,
                    fontWeight: FontWeight.w800,
                  ),
            )),
      );

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(


        actions: [
          IconButton(
          icon: Icon(Icons.clear_rounded),
          onPressed: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => HomePage()),
            );


          },
        ),]

      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center, children: [button()]),
      ),
    );
  }

  ///
  requestSuccessfulPopUpPopUp(BuildContext context) {
    Widget icon() => Container(
          alignment: Alignment.topLeft,
          height: 40,
          width: 40,
          child: Image.asset("assets/ic_emoji.png"),
        );

    Widget HeadingText() => Text(
          "Payment was Successfully paid!",
          style: Theme.of(context).textTheme.headline6!.copyWith(
                color: PRIMARY_TEXT_COLOR_BLACK,
                fontWeight: FontWeight.w800,
              ),
        );

    Widget SubHeadingText() => Text(
          "Be happy with the products",
          style: Theme.of(context).textTheme.subtitle2!.copyWith(
                color: PRIMARY_TEXT_COLOR_BLACK,
                fontWeight: FontWeight.w400,
              ),
          textAlign: TextAlign.start,
        );

    Widget button() => Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: size.width * 1,
            height: size.height * 0.07,
            decoration: ShapeDecoration(
              color: ACCENT_COLOR,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(5.0),
                    bottomRight: Radius.circular(5.0)),
              ),
            ),
            child: new MaterialButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  /*Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                  );*/
                },
                child: Text(
                  "Okay",
                  style: Theme.of(context).textTheme.headline5!.copyWith(
                        color: SECONDARY_COLOR,
                        fontFamily: Constants.FONT_NAME_QUICK_SAND,
                        fontWeight: FontWeight.w800,
                      ),
                )),
          ),
        );

    return Dialog(
      backgroundColor: WHITE_COLOR,
      elevation: DEFAULT_ELEVATION,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: Container(
        height: size.height * 0.4,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(32),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  DefaultSpaceVertical(),
                  icon(),
                  SizedBox(
                    height: DEFAULT_MIN_PADDING,
                  ),
                  HeadingText(),
                  SizedBox(
                    height: DEFAULT_MIN_PADDING,
                  ),
                  SubHeadingText(),
                ],
              ),
            ),
            Expanded(child: SizedBox()),
            button(),
          ],
        ),
      ),
    );
  }
}
