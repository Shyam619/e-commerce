class LoginState<T> {
  LoginStatus? state;
  T? data;
  String? message;

  LoginState.loading(this.message) : state = LoginStatus.LOADING;
  LoginState.completed(this.data) : state = LoginStatus.COMPLETED;
  LoginState.error(this.message) : state = LoginStatus.ERROR;

  @override
  String toString() {
    return "Status : $state \n Message : $message \n Data : $data";
  }
}

enum LoginStatus { LOADING, COMPLETED, ERROR }
