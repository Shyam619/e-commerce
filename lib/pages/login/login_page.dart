import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/common_utils.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/common/shared_pref_util.dart';
import 'package:ecommerce_flutter_app/pages/otp/otp_page.dart';
import 'package:ecommerce_flutter_app/widgets/default_button.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';

import 'login_bloc.dart';
import 'login_state.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var size;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _phoneNumberTextController = new TextEditingController();
  TextEditingController _mailIdTextController = new TextEditingController();
  bool _isErrorShown = false;
  bool _isLoggedIn = true;
  String token = '';

  bool? _editModeUserName = false;
  bool? _editModeMailIdOrMobileNumber = false;

  @override
  void dispose() {
    loginBloc.dispose();
    super.dispose();
  }

  @override
  void initState() {
    loginBloc.init();
    super.initState();
   // saveTokenToDatabase();
  }

  void gotoNextPage(){
    ///changing login status TRUE to keep it in LOGGED IN
    loginBloc.keepLoginStatus(_isLoggedIn);
    print({SharedPrefUtils.getBoolData(Constants.LOGIN_STATUS_KEY)});

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => OtpPage()),
    );
  }


  /// Overall Data  service call
  Widget submitLoginCredentials() => StreamBuilder(
    stream: loginBloc.checkLoginStream,
    builder: (context, AsyncSnapshot<LoginState> snapshot) {
      if (snapshot.hasData) {
        switch (snapshot.data!.state) {

          case LoginStatus.LOADING:
            return Container(
                color: Colors.white12,
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: SECONDARY_COLOR,
                  ),
                ));
            break;

          case LoginStatus.ERROR:
            if(! _isErrorShown )
            {
              SchedulerBinding.instance!
                  .addPostFrameCallback((_) =>
                  CommonUtils.showErrorSnackbar(
                      context, snapshot.data!.message.toString()));
              _isErrorShown = true;
            }else
              return Container();
            break;

          case LoginStatus.COMPLETED:
            SchedulerBinding.instance!
                .addPostFrameCallback((_) => gotoNextPage());
            break;
        }
      } else if (snapshot.hasError) {
        return Center(child: Text(snapshot.error.toString()));
      }
      return Container();
    },
  );


  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
        resizeToAvoidBottomInset: false,

        body:GestureDetector(
          onTap: (){
            FocusScope.of(context).unfocus();
          },
          child: Stack(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(DEFAULT_PADDING),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      inputFieldsContainer(),
                      Expanded(child: SizedBox()),
                      termsText(),
                      SizedBox(height: DEFAULT_PADDING),
                      loginButton(),
                      SizedBox(height: DEFAULT_PADDING),
                    ],
                  ),
                ),
              ),
             // submitLoginCredentials(),
            ],
          ),
        )
    );
  }


  Widget loginHeadingText() => Text(
  Constants.LOGIN_SUB_HEADING_1,
  style: Theme.of(context)
      .textTheme
      .headline6!.copyWith(fontWeight: FontWeight.w300, color:PRIMARY_TEXT_COLOR_BLACK),
  );

  Widget loginSubHeadingText() => Text(
  Constants.LOGIN_SUB_HEADING_2,
  style: Theme.of(context)
      .textTheme
      .bodyText2!.copyWith(fontWeight: FontWeight.w200, color:PRIMARY_TEXT_COLOR_BLACK),
  );


  Widget inputFieldsContainer() => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(height: DEFAULT_PADDING),
      SizedBox(height: DEFAULT_PADDING),
      SizedBox(height: DEFAULT_PADDING),
      SizedBox(height: DEFAULT_PADDING),
      loginHeadingText(),
      SizedBox(height: DEFAULT_PADDING),
      loginSubHeadingText(),
      SizedBox(height: DEFAULT_PADDING),
      SizedBox(height: DEFAULT_PADDING),
      mailIDAndPhoneNumberTextField(),
      changeTextField(),
    ],
  );


  Widget mailIDAndPhoneNumberTextField() => _editModeMailIdOrMobileNumber == false
      ? TextFormField(
    enabled: true,
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.LOGIN_PAGE_EMAIL_ID_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(
          color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "",
      border: new OutlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: ACCENT_COLOR),
      ),

      errorBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.LOGIN_PAGE_EMAIL_ID_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.text,
    maxLines: 1,
    maxLength: 30,
    style: Theme.of(context).textTheme.bodyText1!.copyWith(
        color: SECONDARY_COLOR,
        fontWeight: FontWeight.w700,
        fontSize: 16),
    controller: _mailIdTextController,
  )
      : TextFormField(
    enabled: true,
    autofocus: true,
    decoration: new InputDecoration(
      isDense: true,
      labelText: Constants.LOGIN_PAGE_PHONE_NUMBER_HINT,
      labelStyle: Theme.of(context)
          .textTheme
          .subtitle1!
          .copyWith(
          color: PRIMARY_TEXT_COLOR_BLACK, height: 0.1),
      counterText: "",
      border: new OutlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      enabledBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: PRIMARY_COLOR),
      ),

      focusedBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: ACCENT_COLOR),
      ),

      errorBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),

      focusedErrorBorder: new OutlineInputBorder(
        borderSide: new BorderSide(color: ERROR_COLOR),
      ),
      //fillColor: Colors.green
    ),
    validator: (val) {
      if (val!.length < 3) {
        return Constants.LOGIN_PAGE_PHONE_NUMBER_ERROR;
      } else {
        return null;
      }
    },
    keyboardType: TextInputType.text,
    maxLines: 1,
    maxLength: 30,
    style: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
    controller: _mailIdTextController,
  );

  Widget changeTextField() =>Align(
    alignment: Alignment.bottomRight,
    child: _editModeMailIdOrMobileNumber == false
        ? TextButton(
      child: Text(
        "Use Phone Number",
        style: Theme.of(context)
            .textTheme
            .caption!
            .copyWith(
          color: ACCENT_COLOR,
          fontWeight: FontWeight.w400,
        ),
      ),
      onPressed: () {
        setState(() {
          _editModeMailIdOrMobileNumber = !_editModeMailIdOrMobileNumber!;
        });
      },
    )
        :TextButton(
      child:  Text(
        "Use Email-ID",
        style: Theme.of(context)
            .textTheme
            .caption!
            .copyWith(
          color: ACCENT_COLOR,
          fontWeight: FontWeight.w400,
        ),
      ),
      onPressed: () {
        setState(() {
          _editModeMailIdOrMobileNumber = !_editModeMailIdOrMobileNumber!;
        });
      },
    )
  );

  Widget termsText() =>  Padding(
        padding: const EdgeInsets.symmetric(horizontal: DEFAULT_SPACING),
        child: RichText(
            textAlign: TextAlign.center,
            softWrap: true,
            text: TextSpan(children: [
              TextSpan(
                  style: Theme.of(context)
                      .textTheme
                      .caption!
                      .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
                  text: Constants.TERMS_TEXT),
              TextSpan(
                  style: Theme.of(context)
                      .textTheme
                      .caption!
                      .copyWith(color: ACCENT_COLOR,
                      decoration: TextDecoration.underline),
                  text: Constants.TERMS_TEXT_LINK,
                  recognizer: TapGestureRecognizer()..onTap = () async {

                  }),
              TextSpan(
                  style: Theme.of(context)
                      .textTheme
                      .caption!
                      .copyWith(color: PRIMARY_TEXT_COLOR_BLACK),
                  text: Constants.AND_TEXT),
              TextSpan(
                  style: Theme.of(context)
                      .textTheme
                      .caption!
                      .copyWith(color: ACCENT_COLOR,
                      decoration: TextDecoration.underline),
                  text: Constants.PRIVACY_TEXT_LINK,
                  recognizer: TapGestureRecognizer()..onTap = () async {

                  })
            ])),
      );

  Widget loginButton()=> DefaultButton(
    onPressed: (){
      gotoNextPage();
    },
    size: size,
    buttonText: "Login",
  );

}
