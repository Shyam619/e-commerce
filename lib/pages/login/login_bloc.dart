import 'dart:async';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/shared_pref_util.dart';
import 'package:ecommerce_flutter_app/models/user_details.dart';
import 'package:ecommerce_flutter_app/services/api_model/login_response.dart';
import 'package:ecommerce_flutter_app/services/repository.dart';
import 'package:rxdart/rxdart.dart';

import 'login_state.dart';


class LoginBloc {

  UserDetails userDetails = UserDetails();

  final _repository = Repository();

  PublishSubject<LoginState<LoginResponse>> _loginController = PublishSubject<LoginState<LoginResponse>>();

  Stream<LoginState<LoginResponse>> get checkLoginStream => _loginController.stream;
  StreamSink<LoginState<LoginResponse>> get checkLoginSink => _loginController.sink;


  checkLoginCredentials(String phoneNumber, String mailId ,String token) async {
    checkLoginSink.add(LoginState.loading(Constants.STATE_LOADING));
    try {
      LoginResponse response = await _repository.checkLogin(phoneNumber, mailId ,token);

      if (response.status==null) {
        userDetails = response.data!;
        saveTechnicianDetails();
       // updateTechnicianLanguage();
        checkLoginSink.add(LoginState.completed(response));
      }
      else {
        checkLoginSink.add(LoginState.error(response.message));
      }
    }
    catch (e) {
      checkLoginSink.add(LoginState.error(e.toString()));
      print(e);
    }
  }

  void saveTechnicianDetails() async{
//    SharedPrefUtils.storeIntData(Constants.USER_ID_KEY, userDetails.id);
  //  SharedPrefUtils.storeStringData(Constants.USER_NAME_KEY, userDetails.userName);
  }

  void keepLoginStatus(bool _isLoggedIn) async{
    SharedPrefUtils.storeBoolData(Constants.LOGIN_STATUS_KEY,_isLoggedIn);
  }



 /* void updateTechnicianLanguage()   {
      LocalizationService().changeLocale(technicianDetail.language);
  }*/

  dispose() {
    _loginController.close();
  }

  init() {
  _loginController = PublishSubject<LoginState<LoginResponse>>();
  }

}

final loginBloc = LoginBloc();

