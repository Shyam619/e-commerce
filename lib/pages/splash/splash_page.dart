import 'dart:async';
import 'package:ecommerce_flutter_app/common/asset_path.dart';
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/database/add_data.dart';
import 'package:ecommerce_flutter_app/pages/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:ecommerce_flutter_app/pages/home/home_page.dart';


class SplashPage extends StatefulWidget {
  @override
  State createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with SingleTickerProviderStateMixin {
  var width;
  var height;
  late bool _isLoggedIn;
  String status = "";


  late AnimationController controller;
  late Animation<Offset> offset;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(seconds: 1));
    offset = Tween<Offset>(begin: Offset(2.0, 0.0), end: Offset.zero).animate(controller);
    controller.forward();
   startTimeout();
    addData.initiateDataBase();
    addData.getAllData();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  startTimeout() async {
    var duration = const Duration(seconds: 3);
    return new Timer(duration, gotoLoginPage);
  }

 /* void navigateUser() async{
    //check language from cache memory
    String sharedPreferLanguage =  await SharedPrefUtils.getStringData(Constants.LANGUAGE_KEY) ;
    //sharedPreferLanguage == null ?  LocalizationService().changeLocale("English") : LocalizationService().changeLocale(sharedPreferLanguage);

    // check if user already loggedIn then keep user loggedIn else OnboardMessagePage
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _isLoggedIn = prefs.getBool(Constants.LOGIN_STATUS_KEY);
    print("_isLoggedIn :" + _isLoggedIn.toString());
    Route route = MaterialPageRoute(builder: (context) => _isLoggedIn == null ?  OnboardMessagePage() : HomePage());    //as like activity.finish
    Navigator.pushReplacement(context, route);

  }*/

/*  void handleTimeout() {
    Route route = MaterialPageRoute(builder: (context) => OnboardMessagePage());    //as like activity.finish
    Navigator.pushReplacement(context, route);
  }*/

  void gotoHomePage() {
    Route route = MaterialPageRoute(builder: (context) => HomePage());    //as like activity.finish
    Navigator.pushReplacement(context, route);
  }
  void gotoLoginPage() {
    Route route = MaterialPageRoute(builder: (context) => LoginPage());    //as like activity.finish
    Navigator.pushReplacement(context, route);
  }




  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                  SlideTransition(
                  position: offset,
                  child:
                    Container(
                        height:SPLASH_LOGO_HEIGHT ,
                        width: SPLASH_LOGO_WIDTH,
                        decoration: BoxDecoration(image: DecorationImage(
                          image: AssetImage(AssetsPath.PATH_APP_LOGO),
                        ),)),
                  ),
                    SizedBox(height: DEFAULT_SPACING,)
                  ],
                )
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              width: double.infinity,
              child: new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //new Padding(padding: EdgeInsets.only(bottom: 55.0),),
                    new Text(Constants.VERSION_TEXT + Constants.APP_VERSION,),
                    SizedBox(height: DEFAULT_SPACING_MIN,),
                  ]
              ),
            ),
          )
        ],
      ),
    );
  }
}
