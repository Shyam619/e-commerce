import 'package:carousel_slider/carousel_slider.dart';
import 'package:ecommerce_flutter_app/common/colors.dart';
import 'package:ecommerce_flutter_app/common/constants.dart';
import 'package:ecommerce_flutter_app/common/dimensions.dart';
import 'package:ecommerce_flutter_app/models/Product.dart';
import 'package:ecommerce_flutter_app/pages/product_detail_one/detail_screen.dart';
import 'package:ecommerce_flutter_app/pages/product_list/product_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var size;
  List images = [
    'assets/offer1.jpg',
    'assets/offer2.jpg',
    'assets/offer3.jpg',
    'assets/offer4.jpg'
  ];
  List<String> categoriesLabel = [
    'allCategory',
    'Men',
    'Women',
    'kid',
    'home',
    'electronics',
    'grocery'
  ];

  List<String> offerProduct = [
    "https://cdn11.bigcommerce.com/s-pkla4xn3/images/stencil/1280x1280/products/20743/182602/AODLEE-Fashion-Men-Sneakers-for-Men-Casual-Shoes-Breathable-Lace-up-Mens-Casual-Shoes-Spring-Leather__46717.1545973953.jpg?c=2?imbypass=on",
    "https://cdn11.bigcommerce.com/s-pkla4xn3/images/stencil/1280x1280/products/20743/182602/AODLEE-Fashion-Men-Sneakers-for-Men-Casual-Shoes-Breathable-Lace-up-Mens-Casual-Shoes-Spring-Leather__46717.1545973953.jpg?c=2?imbypass=on",
    "Mens Shoes",
    "45% discount"
  ];

  ///for scroll back-to-top
  bool _showBackToTopButton = false;
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    backToTopControl();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void gotoCategoryPage() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomeScreen(),
      ),
    );
  }

  void backToTopControl() {
    _scrollController = ScrollController()
      ..addListener(() {
        setState(() {
          if (_scrollController.offset >= 80) {
            _showBackToTopButton = true; // show the back-to-top button
          } else {
            _showBackToTopButton = false; // hide the back-to-top button
          }
        });
      });
  }

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: Duration(milliseconds: 100), curve: Curves.linear);
  }

  Widget offerPosts() => CarouselSlider.builder(
      carouselController: CarouselController(),
      itemCount: images.length,
      itemBuilder: (ctx, index, realIdx) {
        return Container(
            height: DASHBOARD_POST_HEIGHT,
            width: double.infinity,
            padding: EdgeInsets.only(right: 0.0, left: 0.0),
            child: CachedNetworkImage(
              height: DASHBOARD_POST_IMG_HEIGHT,
              width: DASHBOARD_POST_IMG_WIDTH,
              alignment: Alignment.center,
              imageUrl:
                  "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg",
              imageBuilder: (context, imageProvider) => Container(
                  height: DASHBOARD_POST_IMG_HEIGHT,
                  width: DASHBOARD_POST_IMG_WIDTH,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                        Radius.circular(DEFAULT_BORDER_RADIUS)),
                    image: DecorationImage(
                      image: NetworkImage(
                          "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"),
                      fit: BoxFit.cover,
                    ),
                  )),
              placeholder: (context, url) =>
                  Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            )

            /*Image(image: AssetImage(images[index]), fit: BoxFit.cover,)*/
            );
      },
      options: CarouselOptions(
        initialPage: 0,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 8),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        enlargeCenterPage: true,
      ));

  Widget miniCategory() {
    Widget categoryCard(String name) {
      var categoryText = Text(
        name,
        style: Theme.of(context).textTheme.subtitle2!.copyWith(
              fontWeight: FontWeight.w500,
              color: PRIMARY_COLOR,
            ),
      );

      return Container(
          padding: EdgeInsets.all(DASHBOARD_THEME_CARD_RADIUS),
          decoration: BoxDecoration(
              border: Border.all(color: SHADOW_COLOR),
              borderRadius:
                  BorderRadius.circular(DASHBOARD_MINI_CATEGORY_BORDER_RADIUS)),
          child: Center(child: categoryText));
    }

    return Expanded(
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: categoriesLabel.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(DASHBOARD_THEME_CARD_RADIUS),
              child: categoryCard(categoriesLabel[index]),
            );
          }),
    );
  }

  Widget popularMenu() {
    var popUpText1 = Text(
      Constants.POPULAR_MENU_TEXT1,
      style: Theme.of(context).textTheme.bodyText2!.copyWith(
            fontWeight: FontWeight.w400,
            color: SHADOW_COLOR,
          ),
    );
    var popUpText2 = Text(
      Constants.POPULAR_MENU_TEXT2,
      style: Theme.of(context).textTheme.bodyText2!.copyWith(
            fontWeight: FontWeight.w400,
            color: SHADOW_COLOR,
          ),
    );
    var popUpText3 = Text(
      Constants.POPULAR_MENU_TEXT3,
      style: Theme.of(context).textTheme.bodyText2!.copyWith(
            fontWeight: FontWeight.w400,
            color: SHADOW_COLOR,
          ),
    );
    var popUpText4 = Text(
      Constants.POPULAR_MENU_TEXT4,
      style: Theme.of(context).textTheme.bodyText2!.copyWith(
            fontWeight: FontWeight.w400,
            color: SHADOW_COLOR,
          ),
    );

    return Padding(
      padding: EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: DASHBOARD_POPULAR_MENU,
                height: DASHBOARD_POPULAR_MENU,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF2F3F7)),
                child: RawMaterialButton(
                  onPressed: () {},
                  shape: CircleBorder(),
                  child: Icon(
                    Icons.account_balance,
                    color: Color(0xFFAB436B),
                  ),
                ),
              ),
              popUpText1,
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: DASHBOARD_POPULAR_MENU,
                height: DASHBOARD_POPULAR_MENU,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF2F3F7)),
                child: RawMaterialButton(
                  onPressed: () {},
                  shape: CircleBorder(),
                  child: Icon(
                    FontAwesomeIcons.clock,
                    color: Color(0xFFC1A17C),
                  ),
                ),
              ),
              popUpText2
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: DASHBOARD_POPULAR_MENU,
                height: DASHBOARD_POPULAR_MENU,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF2F3F7)),
                child: RawMaterialButton(
                  onPressed: () {},
                  shape: CircleBorder(),
                  child: Icon(
                    FontAwesomeIcons.truck,
                    color: Color(0xFF5EB699),
                  ),
                ),
              ),
              popUpText3
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                width: DASHBOARD_POPULAR_MENU,
                height: DASHBOARD_POPULAR_MENU,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF2F3F7)),
                child: RawMaterialButton(
                  onPressed: () {},
                  shape: CircleBorder(),
                  child: Icon(
                    FontAwesomeIcons.gift,
                    color: Color(0xFF4D9DA7),
                  ),
                ),
              ),
              popUpText4
            ],
          )
        ],
      ),
    );
  }

  /*Widget productViews() {
    Widget offersBasedOnCategory() {
      Widget productImage() => CachedNetworkImage(
            height: DASHBOARD_CARD_PRODUCT_SIZE,
            width: double.infinity,
            alignment: Alignment.center,
            imageUrl:
                "https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10331501/2020/3/18/d4d07c60-88d9-43d9-aa78-9cc7712816321584508934272-US-Polo-Assn-Men-White-Colourblocked-Sneakers-84015845089331-1.jpg",
            imageBuilder: (context, imageProvider) => Container(
                width: double.infinity,
                height: DASHBOARD_CARD_PRODUCT_SIZE,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS),
                      topRight:
                          Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS)),
                  image: DecorationImage(
                    image: NetworkImage(
                        "https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10331501/2020/3/18/d4d07c60-88d9-43d9-aa78-9cc7712816321584508934272-US-Polo-Assn-Men-White-Colourblocked-Sneakers-84015845089331-1.jpg"),
                    fit: BoxFit.cover,
                  ),
                )),
            placeholder: (context, url) =>
                Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => Icon(Icons.error),
          );

      var productBrand = Text(
        "Bata",
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.caption!.copyWith(
              fontWeight: FontWeight.w600,
              color: Colors.black54,
            ),
      );

      var productOffer = Text(
        "40% to 50%",
        style: Theme.of(context).textTheme.caption!.copyWith(
              fontWeight: FontWeight.w400,
              color: Colors.red,
            ),
      );

      return Container(
        color: Colors.grey[100], //0xFFF5F5F5
        child: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                "Shoes for Men",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Colors.black54,
                    ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Card(
                        child: Column(
                          children: [
                            productImage(),
                            productBrand,
                            productOffer
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Card(
                        child: Column(
                          children: [
                            productImage(),
                            productBrand,
                            productOffer
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Card(
                        child: Column(
                          children: [
                            productImage(),
                            productBrand,
                            productOffer
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }

    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: 10,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.all(DASHBOARD_THEME_CARD_RADIUS),
            child: offersBasedOnCategory(),
          );
        });
  }*/

  Widget productViews1() {
    var categoryText = Text(
      Constants.CATEGORY_HEADING_TEXT,
      textAlign: TextAlign.center,
      style: Theme.of(context).textTheme.headline6!.copyWith(
            fontWeight: FontWeight.w500,
            color: PRIMARY_COLOR,
          ),
    );

    Widget moreText() => Text(
          Constants.MORE_CATEGORY_HEADING_TEXT,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontWeight: FontWeight.w500,
                color: PRIMARY_COLOR,
              ),
        );

    Widget offersBasedOnCategory(Product singleProduct) {
      Widget productImage() => CachedNetworkImage(
            height: DASHBOARD_CARD_PRODUCT_SIZE,
            width: double.infinity,
            alignment: Alignment.center,
            imageUrl:
                "https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10331501/2020/3/18/d4d07c60-88d9-43d9-aa78-9cc7712816321584508934272-US-Polo-Assn-Men-White-Colourblocked-Sneakers-84015845089331-1.jpg",
            imageBuilder: (context, imageProvider) => Container(
                width: double.infinity,
                height: DASHBOARD_CARD_PRODUCT_SIZE,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS),
                      topRight:
                          Radius.circular(DASHBOARD_PRODUCT_IMAGE_RADIUS)),
                  image: DecorationImage(
                    image: NetworkImage(
                        "https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10331501/2020/3/18/d4d07c60-88d9-43d9-aa78-9cc7712816321584508934272-US-Polo-Assn-Men-White-Colourblocked-Sneakers-84015845089331-1.jpg"),
                    fit: BoxFit.cover,
                  ),
                )),
            placeholder: (context, url) =>
                Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => Icon(Icons.error),
          );

      var productBrand = Text(
        singleProduct.title,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.caption!.copyWith(
              fontWeight: FontWeight.w600,
              color: PRIMARY_COLOR,
            ),
      );

      var productOffer = Text(
        Constants.OFFER_TEXT,
        style: Theme.of(context).textTheme.caption!.copyWith(
              fontWeight: FontWeight.w400,
              color: Colors.red,
            ),
      );

      return Card(
        child: InkWell(
          borderRadius: BorderRadius.circular(MIN_RADIUS),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DetailScreen(product: singleProduct),
              ),
            );
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              productImage(),
              productBrand,
              Expanded(child: productOffer)
            ],
          ),
        ),
      );
    }

    return Padding(
      padding: const EdgeInsets.all(DEFAULT_PADDING_MIN),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [categoryText, moreText()],
        ),
        SizedBox(height: DEFAULT_SIZED_BOX_MIN_HEIGHT),
        GridView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3),
            itemCount: products.length,
            itemBuilder: (BuildContext context, index) {
              return offersBasedOnCategory(products[index]);
            }),
      ]),
    );
  }

  Widget topCategories() {
    var topCategoryHeadingText = Text(
      Constants.TOP_CATEGORY_HEADING_TEXT,
      textAlign: TextAlign.left,
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
            fontWeight: FontWeight.w600,
            color: Colors.black54,
          ),
    );

    Widget imageNameCard(String name) {
      Widget categoryImage() => CachedNetworkImage(
            height: DASHBOARD_CARD_PRODUCT_SIZE,
            width: double.infinity,
            alignment: Alignment.center,
            imageUrl:
                "https://img1.pnghut.com/13/15/0/tjjG8VgLW5/black-motorcycle-helmets-scooter-outline-of-motorcycles-and-motorcycling.jpg",
            imageBuilder: (context, imageProvider) => Container(
                width: double.infinity,
                height: DASHBOARD_CARD_PRODUCT_SIZE,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  image: DecorationImage(
                    image: NetworkImage(
                        "https://img1.pnghut.com/13/15/0/tjjG8VgLW5/black-motorcycle-helmets-scooter-outline-of-motorcycles-and-motorcycling.jpg"),
                    fit: BoxFit.fill,
                  ),
                )),
            placeholder: (context, url) =>
                Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => Icon(Icons.error),
          );
      var categoryName = Text(
        name,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.caption!.copyWith(
              fontWeight: FontWeight.w600,
              color: Colors.black54,
            ),
      );
      return Expanded(
        child: Padding(
          padding: const EdgeInsets.all(DEFAULT_PADDING_MIN),
          child: Column(
            children: [categoryImage(), categoryName],
          ),
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        topCategoryHeadingText,
        Row(children: [
          imageNameCard("Car & Bike"),
          imageNameCard("Electronics"),
          imageNameCard("Personal Care")
        ])
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      body: ListView(
        controller: _scrollController,
        shrinkWrap: true,
        children: [
          SizedBox(height: DEFAULT_SIZED_BOX_HEIGHT),
          offerPosts(),
          SizedBox(height: DEFAULT_SIZED_BOX_HEIGHT),
          Container(height: TOP_CATEGORY_HEADING_TEXT, child: miniCategory()),
          popularMenu(),
          Divider(
            color: Colors.grey.shade400,
            thickness: DEFAULT_DIVIDER_HEIGHT,
          ),
          topCategories(),
          Divider(
            color: Colors.grey.shade400,
            thickness: DEFAULT_DIVIDER_HEIGHT,
          ),
          SizedBox(height: DEFAULT_SIZED_BOX_MIN_HEIGHT),
          /*productViews(),*/

          productViews1(),
          SizedBox(height: DEFAULT_SIZED_BOX_LARGE_HEIGHT),
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: DEFAULT_SIZED_BOX_LARGE_HEIGHT),
        child: _showBackToTopButton == false
            ? null
            : FloatingActionButton(
                backgroundColor: ACCENT_COLOR,
                onPressed: _scrollToTop,
                child: Icon(
                  Icons.arrow_upward,
                  color: SECONDARY_COLOR,
                ),
              ),
      ),
    );
  }
}
